Kaizen Scheduler Core
=====================

* Need a rabbitmq instance running on default port (else modify etc/config.yaml)
* At the moment need to run migrations manually for each DB:

  * :code:`jobs-manage migrate`

* Then can run with:

  * :code:`jobs-manage run`

-----

Architecture:

.. image:: resources/architecture.png
        :alt: Kaizen Architecture
        :width: 100%
        :align: center
