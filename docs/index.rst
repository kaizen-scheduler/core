.. Kaizen documentation master file, created by
   sphinx-quickstart on Wed Sep 21 20:41:59 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Kaizen's documentation!
==================================

Contents:

.. toctree::
   :maxdepth: 2

    Module Reference <source/index>



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

