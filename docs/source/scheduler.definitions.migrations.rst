scheduler.definitions.migrations package
========================================

Submodules
----------

scheduler.definitions.migrations.0001_initial module
----------------------------------------------------

.. automodule:: scheduler.definitions.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.definitions.migrations
    :members:
    :undoc-members:
    :show-inheritance:
