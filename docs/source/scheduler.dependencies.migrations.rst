scheduler.dependencies.migrations package
=========================================

Submodules
----------

scheduler.dependencies.migrations.0001_initial module
-----------------------------------------------------

.. automodule:: scheduler.dependencies.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.dependencies.migrations.0002_auto_20161203_1646 module
----------------------------------------------------------------

.. automodule:: scheduler.dependencies.migrations.0002_auto_20161203_1646
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.dependencies.migrations
    :members:
    :undoc-members:
    :show-inheritance:
