scheduler.jobs.templatetags package
===================================

Submodules
----------

scheduler.jobs.templatetags.job_definitions module
--------------------------------------------------

.. automodule:: scheduler.jobs.templatetags.job_definitions
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.jobs.templatetags
    :members:
    :undoc-members:
    :show-inheritance:
