scheduler.repos.migrations package
==================================

Submodules
----------

scheduler.repos.migrations.0001_initial module
----------------------------------------------

.. automodule:: scheduler.repos.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.repos.migrations.0002_auto_20161218_1903 module
---------------------------------------------------------

.. automodule:: scheduler.repos.migrations.0002_auto_20161218_1903
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.repos.migrations
    :members:
    :undoc-members:
    :show-inheritance:
