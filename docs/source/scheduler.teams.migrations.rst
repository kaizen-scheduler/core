scheduler.teams.migrations package
==================================

Submodules
----------

scheduler.teams.migrations.0001_initial module
----------------------------------------------

.. automodule:: scheduler.teams.migrations.0001_initial
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.teams.migrations
    :members:
    :undoc-members:
    :show-inheritance:
