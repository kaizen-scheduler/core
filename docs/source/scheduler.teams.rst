scheduler.teams package
=======================

Subpackages
-----------

.. toctree::

    scheduler.teams.migrations
    scheduler.teams.settings

Submodules
----------

scheduler.teams.apps module
---------------------------

.. automodule:: scheduler.teams.apps
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.handlers module
-------------------------------

.. automodule:: scheduler.teams.handlers
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.models module
-----------------------------

.. automodule:: scheduler.teams.models
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.serializers module
----------------------------------

.. automodule:: scheduler.teams.serializers
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.urls module
---------------------------

.. automodule:: scheduler.teams.urls
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.views module
----------------------------

.. automodule:: scheduler.teams.views
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.viewsets module
-------------------------------

.. automodule:: scheduler.teams.viewsets
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.teams
    :members:
    :undoc-members:
    :show-inheritance:
