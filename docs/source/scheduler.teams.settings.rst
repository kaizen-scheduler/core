scheduler.teams.settings package
================================

Submodules
----------

scheduler.teams.settings.urls module
------------------------------------

.. automodule:: scheduler.teams.settings.urls
    :members:
    :undoc-members:
    :show-inheritance:

scheduler.teams.settings.views module
-------------------------------------

.. automodule:: scheduler.teams.settings.views
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: scheduler.teams.settings
    :members:
    :undoc-members:
    :show-inheritance:
