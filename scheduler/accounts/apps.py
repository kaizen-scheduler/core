from django.apps import AppConfig


class AccountsConfig(AppConfig):
    name = 'scheduler.accounts'

    def ready(self):
        from scheduler.accounts import handlers  # noqa
