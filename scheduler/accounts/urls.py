from django.conf.urls import include, url
from django.views.generic import RedirectView

import views

app_name = 'accounts'
urlpatterns = [
    url('^', include('django.contrib.auth.urls')),
    url('^profile/$', views.UserDetail.as_view(), name='detail'),
    url('^profile/(?P<pk>[0-9]+)/$', views.UserDetail.as_view(), name='detail'),
    url('^manage/$', RedirectView.as_view(url='users/'), name='manage'),
    url('^manage/users/$', views.ManageUsers.as_view(), name='users'),
    url('^manage/groups/$', views.ManageGroups.as_view(), name='groups'),
]
