from django.contrib.auth.models import Group, User
from django.views import generic


class UserDetail(generic.DetailView):
    model = User

    def get_object(self):
        """ Default the profile page to the currently logged in user """
        self.kwargs.setdefault(self.pk_url_kwarg, self.request.user.id)
        return super(UserDetail, self).get_object()


class ManageGroups(generic.ListView):
    model = Group


class ManageUsers(generic.ListView):
    model = User
