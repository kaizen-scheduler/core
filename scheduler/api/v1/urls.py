from rest_framework import routers

from rest_framework_extensions.routers import NestedRouterMixin
from scheduler.definitions.viewsets import (JobDefinitionFileViewSet,
                                            JobDefinitionViewSet)
from scheduler.directives.viewsets import DirectiveViewSet
from scheduler.filetree.viewsets import NodeViewSet
from scheduler.jobs.viewsets import JobInstanceViewSet, JobViewSet
from scheduler.repos.api_urls import register as register_repos


class NestedRouter(NestedRouterMixin, routers.DefaultRouter):
    pass


router = NestedRouter()
router.register('definitions', JobDefinitionViewSet)
router.register('definition_files', JobDefinitionFileViewSet)
router.register('directives', DirectiveViewSet)
router.register('browse', NodeViewSet)
router.register('jobs', JobViewSet)
router.register('instances', JobInstanceViewSet)

register_repos('repos', router)

urlpatterns = router.urls
