import jinja2
import logging
from django.conf import settings

log = logging.getLogger(__name__)


def preload(ctx):
    """ Preloader for templating configuration values made available
    by a number of configuration providers. """
    # XXX: the providers may in future need more than just the job?
    ctx.definition = apply_templating(ctx.definition, job=ctx.job)
    return ctx


def apply_templating(definition, **kwargs):
    config = _generate_config(definition=definition, **kwargs)
    log.debug("Rendering template with config %s", config)
    return _render_dict(definition, config)


def _render_dict(dictionary, ctx):
    new_dict = {}
    for k, v in dictionary.iteritems():
        if isinstance(v, dict):
            new_dict[k] = _render_dict(v, ctx)
        elif isinstance(v, basestring):
            new_dict[k] = _render(v, ctx)
        else:
            new_dict[k] = v
    return new_dict


def _render(string, ctx):
    """ Take a string, compile it into a template and render it.

    But it's more complicated... we need to be able to return JSON booleans
    and numbers as well as just strings! So we need to inspect the AST to check
    if the top-level node is a int/bool/float filter and if so cast the value.

    We know it's a safe cast because the filter has already cast the other way.
    """
    # By default, cast to a string
    cast = str

    # Parse the AST and check if we're filtering
    ast = ENV.parse(string)
    nodes = ast.body[0].nodes
    if len(nodes) == 1 and isinstance(nodes[0], jinja2.nodes.Filter):
        # We have a top-level filter with no other text/information
        fltr = nodes[0].name
        import __builtin__
        if hasattr(__builtin__, fltr):
            cast = getattr(__builtin__, fltr)
        if fltr == 'int':
            cast = int
        elif fltr == 'float':
            cast = float

    return cast(ENV.from_string(string).render(**ctx))


def _generate_config(**kwargs):
    """ From the preloaders listed in the config file, assemble a config
    dictionary that we can use to render the job definition. """
    providers = {}
    for key, provider in settings.CONFIG['providers'].iteritems():
        mod, func = provider.rsplit(".", 1)
        f = getattr(__import__(mod, fromlist=[func]), func)
        providers[key] = f(**kwargs)
    return providers


ENV = jinja2.Environment(
    # Raise exception if anything is undefined
    undefined=jinja2.StrictUndefined,
)

# Jinja2 doesn't come with a bool filter, so we'll add one
# N.B. only empty strings evaluate to False
ENV.filters['bool'] = lambda bstr: '' if str(bstr) != 'True' else 'True'
