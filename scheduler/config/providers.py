""" A collection of built-in job configuration providers """
import logging
import os
from django.conf import settings
from scheduler.filetree.permissions import filter_readable
from scheduler.jobs.models import JobInstance

log = logging.getLogger(__name__)


def discovery(job=None, **kwargs):
    """ Discover config from another job instance given its path.

    This provider returns a function that takes the job path and finds the
    latest job instance. The 'discovery' section of each job instance event
    is merged with newer values overwriting older values for any duplicate
    keys.

    TODO: Decide in a more deterministic way which "rungroup" to use
    TODO: Allow the job_path to be relative (using os.path.join)

    XXX: Is the job owner the correct user to check permissions against? It kind
    of makes sense if the job is running _setuid_.
    """
    def _discover(job_path):
        abs_job_path = os.path.abspath(os.path.join(os.path.dirname(job.path), job_path))

        # Filter the job instances by ones that are readable based on the owner
        # of the current job.
        jis = filter_readable(
            JobInstance.objects.filter(job__path=abs_job_path).order_by('instance'),
            user=job.user, node_field='job'
        )

        # If no jis exist, either we're not permissioned to view it or it actually
        # just doesn't exist.
        if not jis.exists():
            raise ValueError("Job %s doesn't exist to discover its config" % abs_job_path)

        ji = jis[0]

        # TODO: should this just be states? Or all events?
        # Grab just the discovery information, else empty dictionary
        configs = map(lambda state: (state.json_data or {}).get('discovery', {}), ji.states.all())

        log.debug("Got discovery configs for job path %s: %s", job_path, configs)

        # Then fold over it to take the most recent values
        return reduce(lambda d, acc: dict(acc, **d), configs)
    return _discover


def this(definition=None, **kwargs):
    """ Read values from the same definition.
    Useful to get resolved values like start time, rungroup etc
    """
    return definition


def globals(**kwargs):
    """ Return the globals namespace defined in the config. """
    return settings.CONFIG['globals']
