from django.apps import AppConfig


class DefinitionsConfig(AppConfig):
    name = 'scheduler.definitions'

    def ready(self):
        from scheduler.definitions import handlers  # noqa
