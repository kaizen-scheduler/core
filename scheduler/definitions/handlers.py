import logging
import os

from celery import chain
from django.db import transaction
from django.dispatch import receiver

from scheduler.repos.signals import repo_updated
from scheduler.signals import do_recovery
import tasks
from models import JobDefinition, JobDefinitionFile

log = logging.getLogger(__name__)


@receiver(do_recovery)
def on_recovery(sender, startup=None, **kwargs):
    # Parse any definitions that haven't been parsed in parallel
    tasks.parse_all_definition_files().delay().join()


@receiver(repo_updated)
def on_repo_update(sender, repo=None, commit_hash=None, diffs=None, **kwargs):
    try:
        log.info("Got a repo update signal for %s with %d diffs", repo, len(diffs))
        _on_repo_update(repo, commit_hash, diffs)
    except:
        log.exception("Failed to handle repo update")
        raise


def _on_repo_update(repo, commit_hash, diffs):
    """ When a repo updates there are three types of diffs we need to handle (because we ignore renaming).

    Add)
     * Mark existing JobDefinition files as not active and create a JobDefinition file along with a parsing task.
    Modify)
     * Mark existing JobDefinition files as not active and create a JobDefinition file along with a parsing task.
    Delete)
     * Mark existing JobDefinition files as not active.
    """
    # Get hold of a local git repo for the given JobRepo
    gr = repo.get_git_repo()

    # Diffs is a dict of A, M and D -> [filepath] within the repo since the last known good sync.
    # If the sync is forced, then 'A' includes all files. So we need to be idempotent.
    log.info("Got diffs for repo %s: %s", repo, diffs)

    # We to preserve the atomic nature of a commit, so all JobDefs created/marked from the same sync
    # should be done atomically (although this may in fact include multiple commits)
    defs_to_parse = []
    with transaction.atomic():
        new_paths = diffs['A'] + diffs['M']
        old_paths = diffs['D']

        # For deleted files, mark their associated JobDefinition as active=False
        JobDefinition.objects.filter(file__repo=repo, file__path__in=old_paths).update(active=False)

        for repo_path in new_paths:
            path = os.path.splitext(os.path.join(repo.path, repo_path))[0]
            version, content = gr.file_at_commit(repo_path, commitish=commit_hash)

            # Here is where we're idempotent to force syncs
            job_def, created = JobDefinitionFile.objects.get_or_create(
                repo=repo, path=path, version=version, content=content
            )

            # If we created a new definition then queue it up to be parsed
            if created:
                defs_to_parse.append(job_def)

    # Once the transaction is committed, we can send tasks to process the new definitions
    # TODO are we doing this in the right plcae? Probably not
    if defs_to_parse:
        chain([tasks.parse_job_definition_file.si(jd.id) for jd in defs_to_parse]).delay()
