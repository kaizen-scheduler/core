from django.db import models


class JobDefinitionManager(models.Manager):
    """ A model manager to only return valid job definitions """
    use_for_related_fields = True

    def valid(self):
        return self.get_queryset().filter(is_valid=True)
