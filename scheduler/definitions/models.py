import json
import os

from django.db import models

from managers import JobDefinitionManager
from scheduler.repos.models import Repo
from scheduler.rungroups.models import RunPeriodField, RunGroup
from scheduler.rungroups.schedule import Schedule


class JobDefinitionFile(models.Model):
    """ Model representing a version of a job definition file """

    # Actual string representing the job definition
    content = models.TextField()

    # The repo the job came from
    repo = models.ForeignKey(Repo, related_name='files')

    # The path within the repo definitions_root
    path = models.TextField()

    # The version of the file (usually the git commit hash)
    version = models.CharField(max_length=40)

    # Time we synced the file
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def name(self):
        return os.path.splitext(self.path)[0]

    def __unicode__(self):
        # Take a short string of the version since it's a hash that's unlikely to collide
        return "JobDefinitionFile<%s:%s>" % (self.name, self.version[:6])

    class Meta:
        get_latest_by = 'created_at'
        unique_together = ['repo', 'path', 'version']


class JobDefinition(models.Model):
    """ Model representing a parsed JobDefinitionFile.

    When creating a JobDefinitionFile an async task is spawned to parse and create
    a corresponding JobDefinition.

    This means that any changes to definition parsers won't modify the behaviour
    of old job definitions. This may/may not be what we want but it's nice to have
    the option to freeze the behaviour and we can alway re-parse later.
    """
    # Time the definition was created
    created_at = models.DateTimeField(auto_now_add=True)

    # If the definition has been deleted or there is a newer version then active=False
    active = models.BooleanField(default=True)

    # The raw definition file used to generate this definition
    file = models.OneToOneField(JobDefinitionFile, null=True, related_name='definition')

    # The repo the job belongs to. If file is not None then this == file.repo
    repo = models.ForeignKey(Repo, related_name='definitions')

    # The parsed JSON definition content
    content = models.TextField()

    # A flag to let us know if the definition is valid. If it's not, the content will contain
    # any errors we find during parsing. Not sure I like overloading this, but it'll do for now
    is_valid = models.BooleanField(default=True)

    # The name is parsed from the filepath
    name = models.CharField(max_length=256)

    # Useful to be able to grab definitions by their runperiod for rungroup loading
    runperiod = RunPeriodField(null=True)

    # Override the default manager
    objects = JobDefinitionManager()

    @property
    def warnings(self):
        return json.loads(self.content)['warnings']

    @property
    def errors(self):
        return json.loads(self.content)['errors']

    def diff_to(self, new):
        from difflib import unified_diff
        old, old_v = self.file.content.splitlines(), self.file.version
        new, new_v = new.file.content.splitlines(), new.file.version
        return "\n".join(unified_diff(old, new, lineterm="", fromfile=old_v, tofile=new_v))

    def next_rungroup(self):
        # FIXME: this is actually quite expensive if calendars are used
        s = Schedule(json.loads(self.content))
        for i in range(1, 50):
            r = RunGroup.relative(i, period=self.runperiod)
            if s.matches(r):
                return r

    class Meta:
        get_latest_by = 'file__created_at'
