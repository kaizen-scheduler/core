import json
import logging
import traceback

from django.db import transaction
from scheduler import hooks
from scheduler import rungroups

from models import JobDefinition

log = logging.getLogger(__name__)
PARSE_HOOKS = hooks.get_hooks('parse')


class ParseContext(object):
    """ A ParseContext is the object that is passed to each of the parser's
    plugins to transform raw definitions into compliant JSON objects
    """
    def __init__(self, name, definition, warnings, errors):
        self.name = name
        self.definition = definition

        self.warnings = warnings
        self.errors = errors

    def warning(self, plugin, msg):
        self.warnings.append((plugin, msg))

    def error(self, plugin, msg):
        self.errors.append((plugin, msg))

    def render(self):
        self.definition['name'] = self.name
        self.definition['warnings'] = self.warnings
        self.definition['errors'] = self.errors
        return self.definition


def parse(name, content, repo, file=None):
    """ Parse a definition creating its corresponding JobDefinition """
    parsed_def = _parse_definition(name, content)
    return _create_definition(name, parsed_def, repo, file=file)


def _parse_definition(name, content):
    """ We create a preload context and then apply the preloaders listed in
    the config. We then create a JobDefinition with the parsed definition
    file, marking it valid and active if there are no errors. """
    # Setup warns and errors
    warnings = []
    errors = []

    # First step, parse the JSON (hopefully this works!)
    try:
        definition = json.loads(content)
        if not isinstance(definition, dict):
            raise ValueError("Definition needs to be a JSON object")
    except Exception:
        log.exception("Failed to parse definition %s", name)
        return {
            "errors": [traceback.format_exc()],
            "warnings": []
        }

    def _apply_parser(parse_hook, ctx):
        """ We have to specify a custom apply function so we can catch exceptions
        and insert them into the definition """
        try:
            ctx = parse_hook(ctx)
        except Exception as e:
            log.exception("Failed to apply parser %s: %s", parse_hook.__name__, str(e))
            ctx.errors.append(str(e))
        return ctx

    # Run the parsers
    ctx = ParseContext(name, definition, warnings, errors)
    return hooks.reduce_hooks(PARSE_HOOKS, ctx, apply_func=_apply_parser).render()


@transaction.atomic
def _create_definition(name, parsed_def, repo, file=None):
    """ Create the JobDefinition. If it's valid we need to also mark it
    active and mark the other definitions as inactive.
    """
    # I don't like this... but it makes it much easier to find jobs
    # to load for a given rungroup if we can extract it here
    runperiod = rungroups.period_from_definition(parsed_def)

    valid = len(parsed_def['errors']) == 0

    if valid:
        JobDefinition.objects.filter(name=name).update(active=False)

    job_def = JobDefinition.objects.create(
        name=name, file=file, repo=repo,
        is_valid=valid, runperiod=str(runperiod),
        content=json.dumps(parsed_def)
    )

    return job_def
