from rest_framework import serializers

from models import JobDefinition, JobDefinitionFile


class JobDefinitionSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobDefinition


class JobDefinitionFileSerializer(serializers.ModelSerializer):
    class Meta:
        model = JobDefinitionFile
