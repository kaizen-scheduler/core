import logging
import parser

from celery import group, shared_task

from models import JobDefinitionFile

log = logging.getLogger(__name__)


def parse_all_definition_files():
    """ Parse all definition files that have yet to be parsed """
    def_files = JobDefinitionFile.objects.filter(definition__isnull=True).values_list('id', flat=True)
    log.info("Launching %d parse definition tasks", len(def_files))
    return group([parse_job_definition_file.si(def_file) for def_file in def_files])


@shared_task
def parse_job_definition_file(definition_file_id):
    """ Take a job definition file and parse it to check if it is valid """
    def_file = JobDefinitionFile.objects.get(id=definition_file_id)
    log.info("Parsing job definition file %s", def_file)
    parser.parse(def_file.name, def_file.content, def_file.repo, file=def_file)
