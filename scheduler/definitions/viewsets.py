from rest_framework import viewsets

from models import JobDefinition, JobDefinitionFile
from serializers import JobDefinitionFileSerializer, JobDefinitionSerializer


class JobDefinitionViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = JobDefinition.objects.all()
    serializer_class = JobDefinitionSerializer


class JobDefinitionFileViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = JobDefinitionFile.objects.all()
    serializer_class = JobDefinitionFileSerializer
