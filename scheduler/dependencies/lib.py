"""
This module should be re-implemented as a REST API!!
"""
import logging

from scheduler.dependencies.models import Dependency, Reference

log = logging.getLogger(__name__)


def create_dependencies(reference, deps):
    """
    Using the list syntax, create a set of dependencies e.g.
    'A', [{'B': 'TYPEA'}, {'C': 'TYPEB'}] => A depends on B:TYPEA v C:TYPEB
    'A', [{'B': 'TA'}, {'C': 'TB', 'D': 'TB'}] => A depends on B:TA v (C:TB ^ D:TB)
    where ^ is AND, v is OR
    """
    log.info("Creating dependencies %s for %s", deps, reference)

    # With no deps, just create the reference
    if not deps:
        (ref, _created) = Reference.objects.get_or_create(ref=reference)
        return

    for idx, or_dict in enumerate(deps):
        for depends_on_ref, depends_on_type in or_dict.iteritems():
            # TODO: create multiple
            (ref, _created) = Reference.objects.get_or_create(ref=reference)
            (deps, _created) = Reference.objects.get_or_create(ref=depends_on_ref)
            ref.add_parent(deps, and_idx=idx, state=depends_on_type)


def update_reference(old_ref, new_ref):
    """ Update a reference's dependents to point to a new reference """
    Reference.objects.filter(ref=old_ref).update(ref=new_ref)


def satisfy(reference, dep_type):
    """
    Satisfy a reference
    Returning the list of dependents
    """
    log.debug("Satisfying reference %s in state %s", reference, dep_type)
    deps = Dependency.objects.filter(parent__ref=reference, state=dep_type)
    deps.update(satisfied=True)
    return deps.values_list('child__ref', flat=True)


def is_satisfied(reference):
    """ Is a reference satisfied """
    return Reference.is_satisfied(reference)
