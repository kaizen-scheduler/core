from __future__ import unicode_literals

from django.contrib.postgres.aggregates import BoolAnd
from django.db import connection, models

from django_dag.models import edge_factory, node_factory


class Reference(node_factory('Dependency')):
    """ Just use the ID """
    # Preferably use CharField but need to know max length of
    # stuff...
    # Alternatively just hash stuff?
    ref = models.TextField()

    class Meta:
        app_label = 'dependencies'
        unique_together = ['ref']

    @classmethod
    def dependencies(self, ref):
        deps = self.objects.get(ref=ref).parents()
        return [dep.ref for dep in deps]

    @classmethod
    def dependents(self, ref):
        deps = self.objects.get(ref=ref).children.all()
        return [dep.ref for dep in deps]

    @classmethod
    def all_dependencies(self, ref):
        deps = self.objects.get(ref=ref).ancestors_set()
        return [dep.ref for dep in deps]

    @classmethod
    def all_dependents(self, ref):
        deps = self.objects.get(ref=ref).descendants_set()
        return [dep.ref for dep in deps]

    @classmethod
    def is_satisfied(self, ref):
        if connection.vendor == 'postgresql':
            annotation = BoolAnd('satisfied')
        elif connection.vendor == 'sqlite':
            annotation = models.Min('satisfied')
        else:
            raise NotImplementedError("Don't know how to calculate satisfied for %s", connection.vendor)

        satisfied = Dependency.objects.filter(child__ref=ref).values('and_idx').annotate(
            satisfied__and=annotation
        )

        # Special case for empty dependencies we assume satisfied
        return not satisfied or any([and_idx['satisfied__and'] > 0 for and_idx in satisfied])


class Dependency(edge_factory(Reference, concrete=False)):
    """ Dependency between IDs """
    and_idx = models.IntegerField()
    state = models.CharField(max_length=48, default='SUCCEEDED')
    satisfied = models.BooleanField(default=False)

    class Meta:
        app_label = 'dependencies'
