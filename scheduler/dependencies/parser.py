""" The dependencies parser normalises any dependencies into the type:

    [{Name: State, ...}, ...]

This represents the dependencies of a process in DNF. That is, a disjunction of conjunctive
clauses.  There is currently no algebraic simplification performed
e.g:
    [[A, B], [B, C]] means (A ^ B) v (B ^ C)

A dependency defaults to SUCCEEDED state but can be overridden using a dictionary (since tuples
don't exist in JSON). For example

    [[{A: FAILED}, {B: SUCCEEDED}], [B, C]] means (A failed and B succeeded) or (B succeeded and C succeeded)

Dependencies in the raw process definition can be described in a number of ways:
(where S is short for SUCCEEDED)
    * A => A == A:S == [A] == [[A]] == [{A: S}]
    * A ^ B => [A, B] == [[A, B]] == [[{A: S}, {B: S}]] == [{A: S, B: S}]
    * A v B => [[A], [B]] == [[{A: S}], [{B: S}]] == [{A: S}, {B: S}]
"""
import logging

from scheduler.jobs.states import STATE_SUCCEEDED as S

log = logging.getLogger(__name__)


def parse_dependencies(ctx):
    """ Normalise the dependencies """
    ctx.definition['dependencies'] = _normalise_dependencies(ctx.definition.get('dependencies', []))
    return ctx


def _normalise_dependencies(deps):
    log.debug("Normalising dependencies: %s", deps)

    # If empty string or empty list, just return empty list
    if not deps:
        return []

    # If deps is just a string, make it a list of lists
    if isinstance(deps, basestring):
        return _normalise_dependencies([{deps: S}])

    # If deps is just a dict, make it a list of lists
    if isinstance(deps, dict):
        return _normalise_dependencies([deps])

    # If we get a list of strings then convert into a single conjunction
    if all([isinstance(dep, basestring) for dep in deps]):
        return _normalise_dependencies([{dep: S for dep in deps}])

    # If we get a list of dicts then we're already normalised
    if all([isinstance(dep, dict) for dep in deps]):
        return deps

    raise ValueError("Don't know how to parse %s", deps)
