from django.apps import AppConfig


class DirectivesConfig(AppConfig):
    name = 'scheduler.directives'

    def ready(self):
        from scheduler.directives import handlers  # noqa
