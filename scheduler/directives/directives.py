import logging

from django.shortcuts import get_object_or_404

import tasks
from scheduler.definitions import parser
from scheduler.jobs.models import Job
from scheduler.repos.models import Repo
from scheduler.rungroups.models import RunGroup

log = logging.getLogger(__name__)


def add_job(directive):
    """ Add a job from a definition only """
    # FIXME: Adhoc should be more easily created
    rungroup = RunGroup.from_string("A")
    repo = get_object_or_404(Repo, id=directive.params['repo'])

    # FIXME: verify permissions on the repo

    # Parse and create the job definition
    job_def = parser.parse(
        directive.params['name'], directive.params['definition'], repo
    )

    # Now load the thing!
    return tasks.load_jobs([job_def.job_id], rungroup=rungroup)


def load_rungroup(directive):
    """ Load all jobs for a given rungroup """
    params = directive.params
    rungroup = RunGroup.from_string(params['rungroup'])
    # Grab all the jobs for this runperiod
    job_ids = Job.objects.filter(next_definition__runperiod=rungroup.period.name).values_list('id', flat=True)
    return tasks.load_jobs(job_ids, rungroup)


def load_job(directive):
    """ Load a single job for a given rungroup """
    params = directive.params
    rungroup = RunGroup.from_string(params['rungroup'])
    return tasks.load_jobs([params['job_id']], rungroup, force=params.get('force', False))


def kill_ji(directive):
    """ Kill one or more job instances """
    jids = directive.job_instances.values_list('id', flat=True)
    return tasks.kill_jis(jids)


def run_ji(directive):
    """ Run one or more job instances """
    jids = directive.job_instances.values_list('id', flat=True)
    return tasks.run_jis(jids)
