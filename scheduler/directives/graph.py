from celery.canvas import _chain, group, Signature


def compute_graph(taskset, graph=None):
    """ Compute the task graph of a celery signature.
    For this makes use of only chains and groups.

    Returns the start and end tasks of the taskset
    """
    graph = graph or {}
    taskset.freeze()

    if isinstance(taskset, group):
        return _compute_group_graph(graph, taskset)
    elif isinstance(taskset, _chain):
        return _compute_chain_graph(graph, taskset)
    elif isinstance(taskset, Signature):
        return _compute_single_task_graph(graph, taskset)
    raise ValueError("Unknown taskset type %s", type(taskset))


def _compute_group_graph(graph, taskset):
    # Groups are a list of tasks run in parallel so we add
    # each node attached to the root
    graph.setdefault(taskset.id, [])

    # Our start node is that of the group
    taskset_start = taskset.id

    # Our end nodes is the concatention of all subtask ends
    taskset_ends = []
    for task in taskset.tasks:
        # Always compute the subgraph first to make sure its frozen
        start, ends, graph = compute_graph(task, graph=graph)
        graph[taskset.id].append(start)
        taskset_ends.extend(ends)
    return taskset_start, taskset_ends, graph


def _compute_chain_graph(graph, taskset):
    # Chains run sequentially, so we add an edge from one to the next
    # Though we actually do it backwards
    taskset_start = taskset.id
    taskset_ends = []
    for task in reversed(taskset.tasks):
        # Always compute the subgraph first to make sure its frozen
        start, ends, graph = compute_graph(task, graph=graph)

        # Save the output nodes of the last task
        if not taskset_ends:
            taskset_ends = ends
            taskset_start = start
            continue

        for end in ends:
            # Connect ends to the start of the next task
            graph[end].append(taskset_start)

        taskset_start = start
    return taskset_start, taskset_ends, graph


def _compute_single_task_graph(graph, task):
    # The leaves of our graph are Signatures (or individual tasks)
    # So we actually have nothing to do
    graph[task.id] = []
    return task.id, [task.id], graph
