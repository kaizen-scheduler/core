from django.dispatch import receiver

from scheduler.signals import do_recovery


@receiver(do_recovery)
def on_recovery(sender, startup=None, **kwargs):
    pass
