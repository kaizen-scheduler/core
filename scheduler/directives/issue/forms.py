from django import forms

from scheduler.directives import models


class IssueDirectiveForm(forms.ModelForm):

    class Meta:
        fields = '__all__'
        widgets = {
            'type': forms.HiddenInput()
        }


class AddJobForm(IssueDirectiveForm):
    description = """
    Create a new adhoc job under the given repo and name.
    Note that you must have execute permissions on the repository in order
    to create a job.
    """

    class Meta(IssueDirectiveForm.Meta):
        model = models.AddJobDirective
        exclude = ['job_instances']


class LoadJobForm(IssueDirectiveForm):
    description = """
    Load the given job into the given rungroup. If an instance of the job already
    exists in that rungroup then the load will be skipped unless the force flag is
    set to true.
    """

    class Meta(IssueDirectiveForm.Meta):
        model = models.LoadJobDirective
        exclude = ['job_instances']
        labels = {
            'force': 'Create a new instance even if one already exists'
        }


class LoadRunGroupForm(IssueDirectiveForm):
    description = """
    Load all jobs matching the given rungroup.
    """

    class Meta(IssueDirectiveForm.Meta):
        model = models.LoadRunGroupDirective
        exclude = ['job_instances']


class KillJiForm(IssueDirectiveForm):
    description = """
    Kill the given job instances
    """

    class Meta(IssueDirectiveForm.Meta):
        model = models.KillJiDirective


class RunJiForm(IssueDirectiveForm):
    description = """
    Run the given job instances igoring any defined dependencies
    """

    class Meta(IssueDirectiveForm.Meta):
        model = models.RunJiDirective
