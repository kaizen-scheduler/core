from django.conf.urls import url

import views

app_name = 'issue'
urlpatterns = [
    url(r'^load_rungroup/$', views.IssueLoadRunGroup.as_view(), name='load_rungroup'),

    url(r'^add_job/$', views.IssueAddJob.as_view(), name='add_job'),
    url(r'^load_job/$', views.IssueLoadJob.as_view(), name='load_job'),

    url(r'^kill_ji/$', views.IssueKillJi.as_view(), name='kill_ji'),
    url(r'^run_ji/$', views.IssueRunJi.as_view(), name='run_ji'),
]
