import json

from django.http import HttpResponseRedirect
from django.views import generic

import forms
from scheduler.directives.models import Directive
from scheduler.jobs.models import Job


class IssueDirective(generic.FormView):
    template_name = 'directives/directive_issue.html'
    success_url = '/directives/{id}/'

    def get_initial(self):
        get = {k: v for k, v in self.request.GET.iteritems()}
        get.update({'type': self.directive_type})
        # job_instances is a list, this is a little dirty but until we have
        # another list parameter it'll do.
        get['job_instances'] = self.request.GET.getlist('job_instances', [])
        return get

    def form_valid(self, form):
        """ Create a Directive object and the associated celery task.
        Also convert most of the form into a parameters dictionary
        """
        # Grab the IssueDirective model instance
        issue = form.instance

        # Create a directive then add the M2M relations to JobInstance
        d = Directive.objects.create(
            type=issue.type, parameters=json.dumps(issue.params),
            created_by=self.request.user
        )
        d.job_instances.add(*form.cleaned_data.get('job_instances', []))
        d.run()
        return HttpResponseRedirect(self.success_url.format(**d.__dict__))


class IssueAddJob(IssueDirective):
    form_class = forms.AddJobForm
    directive_type = Directive.ADD_JOB_DIRECTIVE


class IssueLoadRunGroup(IssueDirective):
    form_class = forms.LoadRunGroupForm
    directive_type = Directive.LOAD_RUNGROUP_DIRECTIVE


class IssueLoadJob(IssueDirective):
    form_class = forms.LoadJobForm
    directive_type = Directive.LOAD_JOB_DIRECTIVE

    def get_form(self, **kwargs):
        # FIXME: this really shouldn't be the way to do this
        # Firstly add a custom manager to each model with only permissioned objects
        form = super(IssueLoadJob, self).get_form(**kwargs)
        form.fields['job'].queryset = Job.objects.readable(user=self.request.user)
        return form


class IssueKillJi(IssueDirective):
    form_class = forms.KillJiForm
    directive_type = Directive.KILL_JI_DIRECTIVE


class IssueRunJi(IssueDirective):
    form_class = forms.RunJiForm
    directive_type = Directive.RUN_JI_DIRECTIVE
