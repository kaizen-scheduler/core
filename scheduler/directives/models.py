import json

from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models

from django_celery_results.models import TaskResult
from scheduler.jobs.models import Job, JobInstance
from scheduler.repos.models import Repo
from scheduler.rungroups.models import RunGroupField
import directives
import graph


def require_param(parameter):
    def wrapper(func):
        def func_wrapper(self):
            if parameter not in self.params:
                raise ValidationError("Parameter '%s' required" % parameter)
            return func(self)
        return func_wrapper
    return wrapper


class DirectiveTask(models.Model):
    """ A M2M through table for storing directive tasks """
    directive = models.ForeignKey('Directive')
    result = models.ForeignKey(TaskResult, to_field='task_id', db_constraint=False)

    def __unicode__(self):
        return self.result_id


class Directive(models.Model):
    """ A model representing a directive """

    LOAD_RUNGROUP_DIRECTIVE = 'load_rungroup'

    ADD_JOB_DIRECTIVE = 'add_job'
    LOAD_JOB_DIRECTIVE = 'load_job'

    KILL_JI_DIRECTIVE = 'kill_ji'
    RUN_JI_DIRECTIVE = 'run_ji'

    DIRECTIVES = {
        LOAD_RUNGROUP_DIRECTIVE: 'Load Rungroup',
        ADD_JOB_DIRECTIVE: 'Add Job',
        LOAD_JOB_DIRECTIVE: 'Load Job',
        KILL_JI_DIRECTIVE: 'Kill Job Instance',
        RUN_JI_DIRECTIVE: 'Run Job Instance',
    }

    # Type of directive
    type = models.CharField(max_length=20, choices=DIRECTIVES.items())

    # A directive may operate on many job instance
    job_instances = models.ManyToManyField(JobInstance, blank=True)

    # Each job likely has some parameters as well, store these as generic JSON text
    parameters = models.TextField(default=json.dumps({}))

    # the tasks associated with this directive
    tasks = models.ManyToManyField(TaskResult, through=DirectiveTask)

    # Also store the graph of task completions as JSON
    json_task_graph = models.TextField()

    # When was it issued?
    created_at = models.DateTimeField(auto_now_add=True)

    # And who by?
    created_by = models.ForeignKey(User, related_name='directives')

    def run(self):
        """ Complicated business running directives... """
        taskset = getattr(directives, self.type)(self)
        # Compute the task graph for the directive
        _, _, task_graph = graph.compute_graph(taskset)
        self.json_task_graph = json.dumps(task_graph)
        self.save()

        # Save result objects for every task and add them to the directive
        task_results = [TaskResult(task_id=task_id) for task_id in task_graph]
        TaskResult.objects.bulk_create(task_results)
        d_tasks = [DirectiveTask(directive=self, result=task) for task in task_results]
        DirectiveTask.objects.bulk_create(d_tasks)

        # Finally we can launch the directive
        return taskset.delay()

    @property
    def task_graph(self):
        return json.loads(self.json_task_graph)

    @property
    def result_counts(self):
        """ Return a dictionary of result counts """
        status_counts = self.tasks.values("status").annotate(models.Count('status'))
        return {s['status']: s['status__count'] for s in status_counts}

    @property
    def params(self):
        return json.loads(self.parameters)

    def clean(self):
        """ Make sure we have the right parameters for each type of directive """
        getattr(self, "clean_" + self.type)()

    @property
    def display(self):
        """ Display the parameters nicely for a directive """
        attr = "display_" + self.type

        if hasattr(self, attr):
            return getattr(self, attr)()

        if self.job_instances.count() > 5:
            return "%d job instances" % self.job_instances.count()

        return ', '.join([str(jid) for jid in self.job_instances.values_list('id', flat=True)])

    def display_add_job(self):
        return self.params['name']

    def display_load_rungroup(self):
        return self.params['rungroup']

    def display_load_job(self):
        job = Job.objects.get(id=self.params['job_id'])
        return "%s for %s" % (job, self.params['rungroup'])

    @require_param('name')
    @require_param('definition')
    def clean_add_job(self):
        pass

    @require_param('rungroup')
    def clean_load_rungroup(self):
        pass

    @require_param('rungroup')
    @require_param('job')
    def clean_load_job(self):
        pass

    def __str__(self):
        return "Directive<%d:%s>" % (self.id, self.type)

    class Meta:
        # FIXME
        # ordering = ['-taskset__date_done']
        pass


class AbstractMeta:
    abstract = True
    managed = False


class IssueDirective(models.Model):
    type = models.CharField(max_length=40, choices=Directive.DIRECTIVES.items())
    job_instances = models.ManyToManyField(JobInstance, blank=True)

    @property
    def params(self):
        """ Get the params dictionary for this directive.
        That is every property except type and job_instances. """
        d = {k: v for k, v in self.__dict__.items() if not k.startswith("_")}
        d.pop('type', None)
        d.pop('job_instances', None)
        return d

    class Meta(AbstractMeta):
        pass


class LoadRunGroupDirective(IssueDirective):
    rungroup = RunGroupField()

    class Meta(AbstractMeta):
        pass


class LoadJobDirective(IssueDirective):
    rungroup = RunGroupField()
    job = models.ForeignKey(Job)
    force = models.BooleanField(default=False)

    class Meta(AbstractMeta):
        pass


class AddJobDirective(IssueDirective):
    repo = models.ForeignKey(Repo)
    name = models.CharField(max_length=256)
    definition = models.TextField()

    class Meta(AbstractMeta):
        pass


class KillJiDirective(IssueDirective):
    class Meta(AbstractMeta):
        pass


class RunJiDirective(IssueDirective):
    class Meta(AbstractMeta):
        pass
