import json

from rest_framework import serializers

from models import Directive


class JSONDictSerializerField(serializers.Field):
    """ Serializer for JSONField -- required to make field writable"""

    def to_internal_value(self, data):
        if not isinstance(data, dict):
            raise serializers.ValidationError("Data %s is not a JSON dictionary", data)
        return json.dumps(data)

    def to_representation(self, value):
        repr = json.loads(value)
        if not isinstance(repr, dict):
            raise serializers.ValidationError("Value %s is not a JSON dictionary", value)
        return repr


class DirectiveSerializer(serializers.ModelSerializer):

    parameters = JSONDictSerializerField()

    class Meta:
        model = Directive
        read_only_fields = ('created_by', 'taskset')
