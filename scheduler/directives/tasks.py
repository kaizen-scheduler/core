import logging
import time

from celery import chain, group, shared_task

from scheduler.jobs import instance, states
from scheduler.jobs.models import JobInstance
from scheduler.rungroups.models import RunGroup
from exceptions import InvalidJobStateError

log = logging.getLogger(__name__)


def load_jobs(job_ids, rungroup, force=False):
    # FIXME: prevent loading a rungroup twice. Do this with a rungroup
    # object that tracks the state of teh rungroup. That means the taskset
    # actually needs a start/end taskset to mark the rungroup as LOADING/LOADED
    log.info("Loading %d jobs from runperiod %s for rungroup %s", len(job_ids), rungroup.period.name, rungroup)

    if force:
        # Create a list of (jid, job_id) tuples that we need to kill
        kill_jid_jobs = JobInstance.objects.filter(
            job_id__in=job_ids, rungroup=str(rungroup), instance=0
        ).values_list('id', 'job__id')

        # The jobs we're going to being killing before loading
        kill_jobs = [kill_jid_job[1] for kill_jid_job in kill_jid_jobs]
        non_kill_jobs = set(job_ids) - set(kill_jobs)

        # Use a chain to make sure we only load once we've killed
        kill_loads = [
            chain(_kill_ji.si(jid), _load_job.si(job_id, str(rungroup), force=force))
            for jid, job_id in kill_jid_jobs
        ]
        loads = [_load_job.si(job_id, str(rungroup), force=force) for job_id in non_kill_jobs]

        # Run the whole lot in parallel
        return group(kill_loads + loads)
    else:
        # Return a group of load_job tasksets such that the load taskset ony appears complete
        # once all the load_job tasksets are complete
        return group([_load_job.si(job_id, str(rungroup), force=force) for job_id in job_ids])


def kill_jis(jids):
    log.info("Killing %d job instances: %s", len(jids), jids)
    return group([_kill_ji.si(jid) for jid in jids])


def run_jis(jids):
    log.info("Running %d job instances: %s", len(jids), jids)
    return group([_run_ji.si(jid) for jid in jids])


@shared_task
def _load_job(job_id, rungroup_str, force):
    """ Load a job into the scheduler.

    Basically involves checking if the job matches the rungroup and if so
    to run the preloaders and instantiate a JobInstance
    """
    try:
        rungroup = RunGroup.from_string(rungroup_str)
        return instance.instantiate_job(job_id, rungroup, force=force)
    except:
        log.exception("Failed to load job %s", job_id)
        raise


@shared_task(default_retry_delay=10, max_retries=3, bind=True)
def _kill_ji(self, jid):
    """ Kill the given jid. Then repeat the task until the EXITED
    state exists for the jid.
    TODO: use a celery retry_policy to backoff
    """
    if not instance.has_state(jid, states.STATE_LAUNCHED):
        return

    instance.kill(jid)

    # We'll give it a couple of seconds the first time
    if self.request.retries == 0:
        time.sleep(0.1)

    exited = instance.has_state(jid, states.STATE_EXITED)
    if not exited:
        _kill_ji.retry()


@shared_task
def _run_ji(jid):
    """ Run the given jid """
    if not JobInstance.objects.get(id=jid).state == states.STATE_LOADED:
        raise InvalidJobStateError("Job instance is not in state loaded")
    instance.launch(jid)
