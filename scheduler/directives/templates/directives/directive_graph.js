// create a network
var container = document.getElementById('directive-graph');
var options = {
    groups: {
        SUCCESS: { color:{ background: 'lightgreen' } },
        FAILURE: { color:{ background: 'red' } },
        PENDING: { color:{ background: 'lightgrey' } }
    },
    layout: {
        hierarchical: {direction: 'LR'}
    }
};

$.get("{% url 'directives:graph' directive.id %}")
    .done(function(data) {
        console.log("Got directive graph", data);
        var network = new vis.Network(container, data, options);
        network.setSize("100%", "100%");
        network.redraw();
    })
    .fail(function(error) {
        // TODO: better handle errors here
        console.log("ERROR", error)
    });
