from django.conf.urls import include, url

import views

app_name = 'directives'
urlpatterns = [
    url(r'^$', views.DirectiveList.as_view(), name='list'),
    url(r'^issue/', include('scheduler.directives.issue.urls')),
    url(r'^(?P<pk>[0-9]+)/$', views.DirectiveDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/graph/$', views.vis_graph, name='graph'),
]
