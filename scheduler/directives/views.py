from django.http import JsonResponse
from django.views import generic

from models import Directive


class DirectiveList(generic.ListView):
    model = Directive


class DirectiveDetail(generic.DetailView):
    model = Directive


def vis_graph(request, pk):
    d = Directive.objects.get(id=pk)

    # Create initial nodes and edges
    nodes = []
    edges = []

    def _node_for_task(task):
        return {
            'id': task.task_id,
            'label': task.task_id,
            'group': task.status
        }

    # Build a dictionary of all tasks
    tasks = {t.task_id: t for t in d.tasks.all()}

    # Now traverse the graph
    for task_id in d.task_graph:
        node = _node_for_task(tasks[task_id])
        nodes.append(node)
        edges.extend([
            {'from': task_id, 'to': to_task_id} for to_task_id in d.task_graph[task_id]
        ])

    return JsonResponse({
        "nodes": nodes,
        "edges": edges
    })
