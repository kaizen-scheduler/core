import logging

from celery.result import AsyncResult
from rest_framework import decorators, viewsets
from rest_framework.response import Response

from models import Directive
from serializers import DirectiveSerializer

log = logging.getLogger(__name__)


class DirectiveViewSet(viewsets.ModelViewSet):
    queryset = Directive.objects.all()
    serializer_class = DirectiveSerializer

    @decorators.detail_route(methods=['GET'])
    def status(self, request, pk=None):
        """ Return the status of the given directive """
        taskset_id = Directive.objects.get(id=pk).taskset_id
        result = AsyncResult(taskset_id).status
        return Response({'status': result})

    def perform_create(self, serializer):
        d = serializer.save(created_by=self.request.user)
        d.job_instances.add(*serializer.data.get('job_instances', []))
        d.run()
