import json
from django.db import models
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType


class Event(models.Model):

    # Timestamp the event was created
    timestamp = models.DateTimeField(auto_now_add=True)

    # The generic object we relate to
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = GenericForeignKey('content_type', 'object_id')

    # The event type
    type = models.CharField(max_length=12)

    # The event name
    name = models.CharField(max_length=32)

    # Any additional data for the event
    data = models.TextField(blank=True, null=True)

    @property
    def json_data(self):
        return json.loads(self.data)

    @property
    def pretty_data(self):
        return json.dumps(self.json_data, indent=2) if self.json_data else ""

    def __unicode__(self):
        return self.name

    class Meta:
        get_latest_by = 'timestamp'
