from django.apps import AppConfig


class FileTreeConfig(AppConfig):
    name = 'scheduler.filetree'

    def ready(self):
        from scheduler.filetree import handlers  # noqa
