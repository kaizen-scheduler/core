from models import NodePermission


class NodeBackend(object):

    def authenticate(self, *args, **kwargs):
        return None

    def has_perm(self, user, perm, obj=None):
        if obj is not None:
            return obj.has_perm(user, perm)

    def get_all_permissions(self, user, obj=None):
        """ Returns a set of permission strings that the user has, both
        through group and user permissions. """
        if obj is not None:
            return [perm for perm in NodePermission.PERMS if obj.has_perm(user, perm)]
        return []
