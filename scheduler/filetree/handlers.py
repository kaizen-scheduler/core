from django.contrib.auth.models import User, Group
from django.db.models.signals import post_save
from django.dispatch import receiver
from models import Directory


@receiver(post_save, sender=User)
def on_user_save(sender, instance=None, created=None, **kwargs):
    """ If a superuser is created, then we should setup initial nodes """
    if created and instance.is_superuser:
        # First make sure we have an 'admins' group
        g, _created = Group.objects.get_or_create(name='admin')

        # Make sure the user is part of the admins group
        instance.groups.add(g)

        # Make sure we have a root node
        if not Directory.objects.filter(path="/").exists():
            # Otherwise create one setting the parent to ourselves
            Directory.objects.create(path="/", parent_id="/", user=instance, group=g)
