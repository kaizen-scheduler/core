from django.core.exceptions import PermissionDenied
from models import Node


class PermissionMixin(object):

    def is_allowed(self, node, user=None):
        raise NotImplementedError()

    def get_permission_object(self):
        if hasattr(self, 'permission_object'):
            return self.permission_object
        return (hasattr(self, 'get_object') and self.get_object() or
                getattr(self, 'object', None))

    def dispatch(self, request, *args, **kwargs):
        obj = self.get_permission_object()
        if obj and isinstance(obj, Node):
            if not self.is_allowed(obj, user=request.user):
                raise PermissionDenied()
        return super(PermissionMixin, self).dispatch(request, *args, **kwargs)


class ReadPermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.is_readable(user=user)


class WritePermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.is_writable(user=user)


class ExecutePermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.is_executable(user=user)


class ParentReadPermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.parent.is_readable(user=user)


class ParentWritePermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.parent.is_writable(user=user)


class ParentExecutePermissionMixin(PermissionMixin):
    def is_allowed(self, node, user):
        return node.parent.is_executable(user=user)
