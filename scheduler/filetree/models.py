import os
import stat
from django.contrib.auth.models import User, Group
from django.contrib.contenttypes.models import ContentType
from django.core.exceptions import ValidationError
from django.core.urlresolvers import reverse
from django.db import models
import permissions


def node_path_validator(value):
    # Assert that we have an absolute path
    if not value.startswith("/"):
        raise ValidationError("Node path must be absolute")


class NodePermission(object):
    R = READ = 'read'
    W = WRITE = 'write'
    X = EXECUTE = 'execute'
    PERMS = [READ, WRITE, EXECUTE]


class NodeQuerySet(models.QuerySet):

    def readable(self, user=None):
        return permissions.filter_readable(self, user=user)

    def writable(self, user=None):
        return permissions.filter_writable(self, user=user)

    def executable(self, user=None):
        return permissions.filter_executable(self, user=user)

    def __getitem__(self, key):
        result = super(NodeQuerySet, self).__getitem__(key)
        if isinstance(result, models.Model):
            result = result._as_leaf_class()
        return result

    def __iter__(self):
        for result in super(NodeQuerySet, self).__iter__():
            if isinstance(result, models.Model):
                result = result._as_leaf_class()
            yield result


class Node(models.Model):
    """ A model representing a single node in the file tree.

    Based loosely off the properties found on a Unix inode the filetree node
    repesents a file/directory. The main difference is the name and path are stored
    with the node. This may change in future but for now is a one-to-one mapping
    (i.e. links not allowed) therefore its fine to keep them here. """

    # Full path to the node. We could store the ISDIR info in the mode, but
    # it's more readable to just use this field ending in a slash
    # FIXME: arbitrary max length, could use a TextField if needed
    path = models.CharField(max_length=256, validators=[node_path_validator])

    parent = models.ForeignKey('self', to_field='path', related_name='children')

    # User that owns the node
    user = models.ForeignKey(User)

    # Group that owns the node
    group = models.ForeignKey(Group)

    # The mode/permissions of the node
    mode = models.IntegerField(default=0754)

    # The creation time of the node
    ctime = models.DateTimeField(auto_now_add=True)

    # The ContentType that has subclassed us (if any)
    # Allows for crude subclassed methods for things like custom icons / links
    content_type = models.ForeignKey(ContentType, editable=False, null=True)

    # Custom queryset
    objects = NodeQuerySet.as_manager()
    objects.use_for_related_fields = True

    def __unicode__(self):
        return self.path

    @property
    def ancestors(self):
        parent_paths = []
        curr_path = str(self.path)
        while curr_path:
            parent_paths.append(curr_path)
            curr_path, _end = curr_path.rsplit("/", 1)
        return Node.objects.filter(path__in=parent_paths).order_by('path')

    @property
    def name(self):
        return os.path.basename(self.path)

    def clean(self):
        super(Node, self).clean()
        node_path_validator(self.path)

        # Normalise anything horrible, including removing the trailing slash
        self.path = os.path.normpath(self.path)

        # Make sure our parent_id is our parent directory
        self.parent_id = os.path.dirname(self.path)

        # Set our content type if it doesn't already exist
        if not self.content_type:
            self.content_type = ContentType.objects.get_for_model(self.__class__)

    def save(self, mkdirs=False, **kwargs):
        # First clean ourselves up a bit
        self.clean()

        # Skip all these checks if we're creating the root
        if self.path == "/":
            return super(Node, self).save(**kwargs)

        # Check if our parent exists
        if not Node.objects.filter(path=self.parent_id).exists():
            # If we're not told to mkdirs then raise DoesNotExist
            if not mkdirs:
                raise Node.DoesNotExist(self.parent_id)

            # Otherwise we create the parent
            grandparent = os.path.dirname(self.parent_id)
            n = Directory(
                path=self.parent_id, parent_id=grandparent,
                user=self.user, group=self.group, mode=self.mode
            )
            n.save(mkdirs=True)

        # Now we know we have a parent, we can check if its a directory and writable
        if not self.parent.is_directory():
            raise Node.NotADirectory(self.parent)

        if not self.parent.is_writable(user=self.user):
            raise Node.PermissionDenied(self.parent)

        return super(Node, self).save(**kwargs)

    def is_directory(self):
        return stat.S_ISDIR(self.mode)

    def has_perm(self, user, perm):
        if perm == NodePermission.READ:
            return self.is_readable(user=user)
        if perm == NodePermission.WRITE:
            return self.is_writable(user=user)
        if perm == NodePermission.EXECUTE:
            return self.is_executable(user=user)

    def is_readable(self, user=None):
        perms = (stat.S_IRUSR, stat.S_IRGRP, stat.S_IROTH)
        return self._check_permission(perms, user=user)

    def is_writable(self, user=None):
        perms = (stat.S_IWUSR, stat.S_IWGRP, stat.S_IWOTH)
        return self._check_permission(perms, user=user)

    def is_executable(self, user=None):
        perms = (stat.S_IXUSR, stat.S_IXGRP, stat.S_IXOTH)
        return self._check_permission(perms, user=user)

    def icon(self):
        return "folder" if self.is_directory() else "file outline"

    def link(self):
        return reverse('filetree:browse', kwargs={'path': self.path})

    def _check_permission(self, (u, g, o), user=None):
        if self.mode & o:
            # World readable
            return True

        if not user:
            # Shortcut if no user is given
            return False

        if self.mode & u and user == self.user:
            # User readable
            return True

        if self.mode & g and user.groups.filter(id=self.group_id).exists():
            # Group readable
            return True

        return False

    def _as_leaf_class(self):
        """ Return ourselves wrapped as the subclass, though without hitting the DB.
        This means we have access to subclassed methods, but not to any fields on
        the subclass. """
        model = self.content_type.model_class()
        if self.__class__ == Node and model != Node:
            result = model(node_ptr=self)
            result.__dict__.update(self.__dict__)
            return result
        return self

    class PermissionDenied(Exception):
        pass

    class NotADirectory(Exception):
        pass

    class Meta:
        ordering = ['path']
        unique_together = ['path']


class Directory(Node):

    def __init__(self, *args, **kwargs):
        # Set default mode to 0754
        kwargs.setdefault('mode', stat.S_IFDIR | 0754)

        # Make sure the mode is set to directory
        if 'mode' in kwargs:
            kwargs['mode'] |= stat.S_IFDIR

        super(Directory, self).__init__(*args, **kwargs)

    class Meta:
        proxy = True
