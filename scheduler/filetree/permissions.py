import stat
from django.db.models import F, Q


def filter_readable(qs, user=None, node_field=None):
    """ Filter readable objects from a given queryset """
    return _filter_permission(qs, user, node_field, stat.S_IRUSR, stat.S_IRGRP, stat.S_IROTH)


def filter_writable(qs, user=None, node_field=None):
    """ Filter writable objects from a given queryset """
    return _filter_permission(qs, user, node_field, stat.S_IWUSR, stat.S_IWGRP, stat.S_IWOTH)


def filter_executable(qs, user=None, node_field=None):
    """ Filter executable objects from a given queryset """
    return _filter_permission(qs, user, node_field, stat.S_IXUSR, stat.S_IXGRP, stat.S_IXOTH)


def _filter_permission(qs, user, node_field, usr, grp, oth):
    """ Filter the given objects (accessed via node_field on the given qs) using the
    usr, grp and oth permissions for the given user """
    mode_field = node_field + '__mode' if node_field else 'mode'
    user_field = node_field + '__user' if node_field else 'user'
    group_field = node_field + '__group' if node_field else 'group'

    qs = qs.annotate(
        mode_usr=F(mode_field).bitand(usr)
    ).annotate(
        mode_grp=F(mode_field).bitand(grp)
    ).annotate(
        mode_oth=F(mode_field).bitand(oth)
    )

    # Check for world-<perm>
    q = Q(mode_oth__gt=0)

    if user:
        # Or it's user <perm> and we're the owner
        q |= Q(mode_usr__gt=0, **{user_field: user})

        # Or it's group <perm> and we're a member of the group
        # Note this won't work for transitive groups, but since neither Unix
        # or Django support that, then we should be ok
        q |= Q(mode_grp__gt=0, **{group_field + '__in': user.groups.all()})

    return qs.filter(q)
