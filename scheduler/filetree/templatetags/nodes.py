import stat
from django import template

register = template.Library()


_filemode_table = (
    ((stat.S_IFLNK,                "l"),
     (stat.S_IFREG,                "-"),
     (stat.S_IFBLK,                "b"),
     (stat.S_IFDIR,                "d"),
     (stat.S_IFCHR,                "c"),
     (stat.S_IFIFO,                "p")),

    ((stat.S_IRUSR,                "r"),),
    ((stat.S_IWUSR,                "w"),),
    ((stat.S_IXUSR | stat.S_ISUID, "s"),
     (stat.S_ISUID,                "S"),
     (stat.S_IXUSR,                "x")),

    ((stat.S_IRGRP,                "r"),),
    ((stat.S_IWGRP,                "w"),),
    ((stat.S_IXGRP | stat.S_ISGID, "s"),
     (stat.S_ISGID,                "S"),
     (stat.S_IXGRP,                "x")),

    ((stat.S_IROTH,                "r"),),
    ((stat.S_IWOTH,                "w"),),
    ((stat.S_IXOTH | stat.S_ISVTX, "t"),
     (stat.S_ISVTX,                "T"),
     (stat.S_IXOTH,                "x"))
)


@register.filter
def filemode(mode):
    """ Borrowed from Python3 os.stat.filemode

    Convert a file's mode to a string of the form '-rwxrwxrwx'."""
    perm = []
    for table in _filemode_table:
        for bit, char in table:
            if mode & bit == bit:
                perm.append(char)
                break
        else:
            perm.append("-")

    return "".join(perm)


@register.filter
def can_read(user, node):
    return node.is_readable(user=user)


@register.filter
def can_write(user, node):
    return node.is_writable(user=user)


@register.filter
def can_execute(user, node):
    return node.is_executable(user=user)
