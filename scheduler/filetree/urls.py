from django.conf.urls import url
from views import BrowseNodesView

app_name = 'filetree'
urlpatterns = [
    url(r'^(?P<path>.*)$', BrowseNodesView.as_view(), name='browse'),
]
