from django.views.generic.detail import DetailView
from mixins import ReadPermissionMixin
from models import Node


class BrowseNodesView(ReadPermissionMixin, DetailView):
    model = Node
    slug_field = "path"
    slug_url_kwarg = "path"
    template_name = "filetree/browse.html"

    def get_context_data(self, path=None, **kwargs):
        context = super(BrowseNodesView, self).get_context_data(**kwargs)
        context.update({'children': self.object.children.all()})
        return context
