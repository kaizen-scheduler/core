from django.conf import settings


def get_hooks(name):
    """ Return the list of functions registered for the given hook name.

    It is recommended to use this function at the module level such that
    warnings about missing hooks are thrown at startup and not runtime. """
    funcs = settings.CONFIG.get('hooks', {}).get(name, [])
    return map(_func_from_string, funcs)


def reduce_hooks(hooks, initial_arg, apply_func=None):
    """ Given a list of hooks reduce them using the given argument and
    optional apply_func. """
    def _apply(arg, func):
        if apply_func is not None:
            return apply_func(func, arg)
        return func(arg)
    return reduce(_apply, hooks, initial_arg)


def _func_from_string(func_str):
    mod, func = func_str.rsplit(".", 1)
    return getattr(__import__(mod, fromlist=[func]), func)
