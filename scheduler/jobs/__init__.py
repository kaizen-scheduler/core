default_app_config = 'scheduler.jobs.apps.JobsConfig'

# This will make sure the app is always imported when
# Django starts so that shared_task will use this app.
from scheduler.celeryconf import app as celery_app  # noqa
