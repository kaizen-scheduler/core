from django.apps import AppConfig


class JobsConfig(AppConfig):
    name = 'scheduler.jobs'

    def ready(self):
        from scheduler.jobs import handlers  # noqa
