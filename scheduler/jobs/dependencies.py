"""
Parse and manage process dependencies

Note: need to use absolute_import since we're importing a
package called dependencies into a file with the same name
"""
import logging

from django.db.models import Q

from scheduler.dependencies import lib
from scheduler.events.models import Event
from scheduler.jobs.models import JobInstance

log = logging.getLogger(__name__)


def is_satisfied(ji):
    """ Check if the given PI is satisfied """
    sat = lib.is_satisfied(_to_reference(ji))
    log.info("Ji satisfied: %s", sat)
    return sat


def satisfy(ji, state='SUCCEEDED'):
    """ Satisfy the given ji and state returning a list of dependents """
    refs = lib.satisfy(_to_reference(ji), state)

    if not refs:
        return []

    qs = [_ji_from_reference(ref) for ref in refs]
    return JobInstance.objects.filter(reduce(lambda a, x: a | x, qs))


def register(ji):
    """ Register the dependencies for a given ProcInstance.
    FIXME: be more explicit, take dependencies not ji as arg
    """
    return create_dependencies(ji, ji.resolved.get('dependencies', []))


def create_dependencies(ji, dependencies):
    """
    FIXME: these docs are outdated
    Create the given dependencies, trying to avoid race conditions with previously
    undeclared dependencies satisfying whilst we do this!

    1. Create dependencies
    2. Check if any are satisfied (in states table)
    3. If all satisfied, send an ALL_SAT

    FIXME: Support different state dependencies, for now just SUCCEEDED is supported.
    This should be declared at a finer grained level so I can e.g. declare a dependency
    on A:SUCCEEDED OR B:FAILED etc...
    """
    # Create the dependencies for the process
    log.info("Creating dependencies for %s: %s", ji, dependencies)
    lib.create_dependencies(_to_reference(ji), dependencies)

    # Check if any are satisfied, if so tell the dependencies lib about them
    # Importantly, if any of our dependencies are already satisfied we should mark them as such.
    for state, ji_refs in _get_state_dependencies_dict(dependencies).iteritems():

        # Create a list of state query objects
        qs = [_state_from_reference(ji_ref) for ji_ref in ji_refs]

        # TODO: filter JobState using a list of ji_to_references
        # Find the jis that have been in this state (i.e. satisfied)
        satisfied_states = Event.objects.filter(
            reduce(lambda a, x: a | x, qs), name=state
        ).select_related('instance')

        satisfied_jis = [state.instance for state in satisfied_states]

        log.debug("Got already satisfied jis: %s", satisfied_jis)

        # Because they may have been satisfied before we created them above
        # the event driven mechanism breaks (as they're unknown until now).
        # Therefore we now need to satisfiy the dependencies post-hoc
        # TODO: we can do this in bulk
        [lib.satisfy(_to_reference(j), state) for j in satisfied_jis]


def _to_reference(ji):
    """ Given a JobInstance, produce a unique reference that we can
    use to refer to it. Potentially before it's even been created in
    the case that dependencies are declared out of order.

    A hash of these would also work and have the benefit of being fixed
    with but for now having readable logs wins.

    FIXME: use a pickled namedtuple
    """
    return "%s:%s" % (ji.job_id, ji.rungroup)


def _ji_from_reference(ref):
    """ Convert reference to a query object for a JobState"""
    (job_id_str, rungroup) = ref.split(":")
    return Q(job_id=int(job_id_str), rungroup=rungroup, instance=0)


def _state_from_reference(ref):
    """ Convert reference to a query object for a JobState"""
    (job_id_str, rungroup) = ref.split(":")
    return Q(instance__job_id=int(job_id_str), instance__rungroup=rungroup, instance__instance=0)


def _get_state_dependencies_dict(deps_def):
    """ Create a map of state to set of ji_refs that are dependencies """
    states = dict()
    for and_dict in deps_def:
        for ref, state in and_dict.iteritems():
            states.setdefault(state, set())
            states[state].add(ref)
    return states
