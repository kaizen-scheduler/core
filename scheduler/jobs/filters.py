import django_filters

from models import JobInstance


class JobInstanceFilter(django_filters.FilterSet):
    path = django_filters.CharFilter(name='job__path', lookup_expr='regex')

    # TODO: make this multiple tags with the option to AND/OR them
    # TODO: This only works if resolved_definition is a JSONField
    # tag = django_filters.CharFilter(name='resolved_definition__tags', lookup_expr='contains')

    class Meta:
        model = JobInstance
        fields = ['rungroup', 'state']
