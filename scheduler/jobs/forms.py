from django import forms
from django.core.validators import RegexValidator

from models import JobInstance


class JobInstanceSearchForm(forms.ModelForm):

    path = forms.CharField(validators=[RegexValidator])

    def __init__(self, *args, **kwargs):
        super(JobInstanceSearchForm, self).__init__(*args, **kwargs)

        # It's a search form, everything should be optional!
        for field in self.fields:
            self.fields[field].required = False

    class Meta:
        model = JobInstance
        fields = ['path', 'rungroup']
