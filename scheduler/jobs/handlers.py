import logging
from django.db.models.signals import post_save
from django.dispatch import receiver

import instance
from models import Job
from scheduler.definitions.models import JobDefinition
from scheduler.rungroups import timings
from scheduler.signals import do_recovery
from scheduler.timings.signals import timing_fired

log = logging.getLogger(__name__)


@receiver(do_recovery)
def on_recovery(sender, startup=None, **kwargs):
    """ For any job in a state that is waiting on the router we query
    the router for the job status """
    pass


@receiver(timing_fired)
def on_timing_fired(sender, msg=None, time=None, **kwargs):
    """ Whenever a timing is reached we are notified of the time and message """
    if 'jid' not in msg:
        # Make sure the timing is relevant
        return

    jid = msg['jid']
    timing_type = msg['type']
    log.info("Timing event %s at %s satisfied for %s", timing_type, time, jid)

    if timing_type == timings.START_AFTER:
        instance.on_start_after(jid)
    else:
        log.warning("Unknown timing event %s for jid %d", timing_type, jid)


@receiver(post_save, sender=JobDefinition)
def on_job_def_save(sender, instance=None, created=None, **kwargs):
    if not created:
        pass

    # When a new job definition is created we need to update/create the job for it!
    try:
        job = Job.objects.get(path=instance.name)
        job.next_definition = instance
        job.save()
    except Job.DoesNotExist:
        Job(
            path=instance.name, user=instance.repo.user, group=instance.repo.group,
            job_repo=instance.repo, next_definition=instance
        ).save(mkdirs=True)
