import json
import logging

from django.db import transaction

import dependencies
import states
import tasks
import timings
from models import Job, JobInstance
from scheduler import hooks, preload
from scheduler.rungroups.schedule import Schedule
from scheduler.rungroups.filters import NotInSchedule

log = logging.getLogger(__name__)
PRELOAD_HOOKS = hooks.get_hooks('preload')


@transaction.atomic
def instantiate_job(job_id, rungroup, force=False):
    """ Instantiate a JobInstance from a Job """
    job = Job.objects.get(id=job_id)

    # Get the latest, valid job definition for the job
    job_def = job.next_definition
    definition = json.loads(job_def.content)

    exists = job.instances.filter(rungroup=str(rungroup)).exists()

    if exists and not force:
        # If a ji exists and we're not forcing it, then we can just skip. Useful for when we
        # load a rungroup multiple times for example
        log.warning("Skipping loading job %s for rungroup %s as one already exists", job, rungroup)
        return
    else:
        # Forcing means we have to bump the instance numbers of all of the current JobInstances
        # FIXME: what happens here if the previous instance is running? Hmmm
        job.bump_instance_numbers(rungroup)

    log.info("Loading job %s for rungroup %s", job, rungroup)

    # Run the config providers
    # FIXME: moved to preloader
    # definition = config.apply_templating(definition, job=job, rungroup=rungroup)

    # Run the preloaders
    resolved_definition = _apply_preloaders(job, definition, rungroup)

    log.debug("Got resolved definition: %s", resolved_definition)

    ji = JobInstance.objects.create(
        # TODO: Rungroup should be a slug-related foreign key
        job=job, definition=job_def, rungroup=str(rungroup),
        resolved_definition=json.dumps(resolved_definition, sort_keys=True)
    )

    if resolved_definition['errors']:
        # If we do have any errors then we kinda need to do something about it
        # How about mark it as LOAD_FAILED
        ji.mark_state(states.STATE_LOAD_FAILED)
        # I mean we could do this, but who in their right mind would depend
        # on a job failing to load...
        # tasks.process_dependencies.delay(ji.id, states.STATE_LOAD_FAILED)
    else:
        # Otherwise we're good to register the proc and attempt a launch

        # Register proc dependencies
        dependencies.register(ji)
        timings.register(ji)

        # Mark the new process instance as loaded
        ji.mark_state(states.STATE_LOADED)

        # Let everyone who has a LOADED dependency on us know about it
        tasks.process_dependencies.delay(ji.id, states.STATE_LOADED)

        # Check if the run schedule says we're to run in this rungroup
        try:
            Schedule(resolved_definition).assert_matches(rungroup)
        except NotInSchedule as e:
            ji.mark_state(states.STATE_SKIPPED, data=str(e))
            tasks.process_dependencies.delay(ji.id, states.STATE_SKIPPED)

    return ji.id


def has_state(jid, state):
    """ Has the given jid been in the given state? """
    ji = JobInstance.objects.get(id=jid)
    return ji.states.filter(name=state)


def kill(jid):
    """ Send a kill message for the given jid
    FIXME: refactor so this isn't just a passthrough...
    """
    tasks.kill(jid)


def launch(jid):
    """ Launch the given jid without checking dependencies """
    tasks.launch(jid)


def on_start_after(jid):
    """ If the start_after is hit, we try and launch the ji """
    tasks.maybe_launch.apply(args=(jid,))


def _apply_preloaders(job, definition, rungroup):
    warnings = []
    errors = []

    ctx = preload.PreloadContext(job, definition, rungroup, warnings, errors)
    ctx = hooks.reduce_hooks(PRELOAD_HOOKS, ctx)

    # Grab the definition as it stands now, updating warnings/errors appropriately
    resolved_definition = ctx.definition
    resolved_definition['warnings'].extend(ctx.warnings)
    resolved_definition['errors'].extend(ctx.errors)

    return resolved_definition
