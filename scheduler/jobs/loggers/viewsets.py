import logging
from django.http.response import HttpResponse, Http404
from rest_framework.decorators import detail_route
from rest_framework.response import Response
from scheduler.jobs.models import JobInstance
from scheduler.routers.config import get_router

log = logging.getLogger(__name__)


class LoggingAPIMixin(object):
    """ A mixin for providing the logging endpoints on a JobInstance """

    @detail_route(methods=['get', 'put'])
    def logs(self, request, pk=None):
        ji = JobInstance.objects.get(id=pk)
        if request.method == 'GET':
            return _get_logs(ji)
        elif request.method == 'PUT':
            data = request.data['data']
            return _put_logs(ji, data)


def _get_logs(ji):
    try:
        data = get_router(ji.resolved).logging_backend(ji.resolved).read(ji)
    except IOError as e:
        # Return a 404 if we get any sort of io error
        raise Http404(e)
    return HttpResponse(data, content_type='text/plain')


def _put_logs(ji, data):
    logger = get_router(ji.resolved).logging_backend(ji.resolved)
    logger.write(ji, data)
    return Response()
