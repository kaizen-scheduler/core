from django.core.management import BaseCommand

from scheduler.jobs.models import JobInstance


class Command(BaseCommand):
    help = "List processes"

    def handle(self, *args, **kwargs):
        for p in JobInstance.objects.select_related('definition').order_by('-id')[:15]:
            latest_state = p.states.latest()
            print p.id, latest_state, latest_state.timestamp, p.rungroup, p.definition.name
