import logging
import threading
import time

from django.core.management import BaseCommand

from scheduler import signals
from scheduler.jobs import webserver, worker
from scheduler.timings.poll import TimingsPoller

log = logging.getLogger(__name__)


CORE_SERVICES = [
    worker.CeleryWorker(),
    webserver.SchedulerWebServer(),
]

SERVICES = [
    TimingsPoller(),
]


class Command(BaseCommand):
    help = "Run the scheduler services"

    def handle(self, *args, **kwargs):
        log.info("Starting CORE services...")
        for service in CORE_SERVICES:
            service.daemon = True
            service.start()

        # Send a startup do_recovery signal
        log.info("Performing startup recovery...")
        signals.do_recovery.send(sender=None, startup=True)

        log.info("Starting remaining services...")
        for service in SERVICES:
            service.daemon = True
            service.start()

        while threading.active_count() > 0:
            time.sleep(1)
