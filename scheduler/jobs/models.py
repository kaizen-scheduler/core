import json

from django.contrib.contenttypes.fields import GenericRelation
from django.core.urlresolvers import reverse
from django.db import models, transaction

from scheduler.definitions.models import JobDefinition
from scheduler.events.models import Event
from scheduler.filetree.models import Node, NodeQuerySet
from scheduler.repos.models import Repo


class Job(Node):
    """ A Job can be considered the thing that is instantiated when each rungroup loads.
    The Job is associated with a number of definition files and be default is instantiated
    with the last known good file.
    """
    # The repo the job came from, or null if the job is adhoc
    job_repo = models.ForeignKey(Repo, related_name='jobs', blank=True, null=True)

    # Whether or not the Job is active. A job is marked inactive when it is deleted
    # from the repository
    is_active = models.BooleanField(default=True)

    # The definition to use next time we instantiate the job. Users can override this.
    next_definition = models.ForeignKey(JobDefinition, related_name='_job', null=True)

    objects = NodeQuerySet.as_manager()

    @property
    def repo_path(self):
        """ Returns the path relative to the job's repo """
        if self.job_repo:
            return self.path[len(self.job_repo.path) + 1:]
        return self.path

    def bump_instance_numbers(self, rungroup):
        """ Bump the instance number of each of our job instances for the given rungroup.
        TODO: perhaps this should be written on a JobInstance manager, that way it can be applied
        in bulk on any queryset? """
        # FIXME: remove str(rungroup) to rungroup when rungroup becomes a foreign key
        # We need to order by instance DESC to make sure we bump in the right order
        for instance in self.instances.filter(rungroup=str(rungroup)).order_by('-instance'):
            instance.instance += 1
            instance.save()

    def icon(self):
        return 'terminal'

    def link(self):
        return reverse('jobs:job_detail', args=(self.id,))

    class Meta:
        ordering = ('path',)


class JobInstance(models.Model):

    E_STATE = "STATE"
    E_MSG = "MSG"

    # Job that this is an instance of
    job = models.ForeignKey(Job, related_name="instances")

    # Definition the instance is using
    definition = models.ForeignKey(JobDefinition, related_name="instances")

    # Resolved definition job is instantiated with (e.g. rungroup not runperiod)
    # TODO: we could make this a JSONField however it's postgres specific. Once
    # it becomes clear whether or not there is a limited number of fields we might want
    # to search on then we should extract those into columns.
    resolved_definition = models.TextField()

    # Run Group String
    # TODO: use a StubForeignKEy to RunGroup model
    rungroup = models.CharField(max_length=40)

    # Instance number with 0 always being the latest instance. Sounds kinda stupid, but actually if
    # we created new instances with an increasing number then the DB query to get the active (latest)
    # instances gets quite expensive / complicated
    # (see http://stackoverflow.com/questions/586781/postgresql-fetch-the-row-which-has-the-max-value-for-a-column)
    #
    # Instead, if we bump the numbers attached to the old instances then we pay a cost only when we instantiate a
    # new instance (for the same rungroup remember) which is much less frequent than viewing the latest instances.
    instance = models.IntegerField(default=0)

    # We store a reference the router gave us (this may even be the jid) so multiple schedulers can talk to
    # the router letting it assign unique IDs.
    router_id = models.CharField(max_length=128, null=True)

    # The current state of the job
    # TODO: use a states.forms.StateField
    state = models.CharField(max_length=20)

    # The events associated with the job instance
    events = GenericRelation(Event, related_query_name='job_instance')

    @property
    def resolved(self):
        return json.loads(self.resolved_definition)

    @property
    def states(self):
        return self.events.filter(type=self.E_STATE)

    def mark_event(self, event_type, name, data=None):
        Event(content_object=self, type=event_type, name=name, data=json.dumps(data)).save()

    def mark_msg(self, msg):
        self.mark_event(self.E_MSG, name="Message", data=msg)

    @transaction.atomic
    def mark_state(self, state, data=None):
        self.mark_event(self.E_STATE, name=state, data=data)
        self.state = state
        self.save()

    def __unicode__(self):
        return "%s:%s" % (self.job.path, self.rungroup)

    class Meta:
        unique_together = ['job', 'rungroup', 'instance']
        # TODO: this isn't very nice
        get_latest_by = 'id'
        ordering = ['instance']
