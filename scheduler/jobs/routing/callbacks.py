from scheduler.jobs import tasks, states


def running(ji, data=None):
    """ Notification that the job has been launched by the router """
    ji.mark_state(states.STATE_RUNNING, data)
    tasks.process_dependencies.delay(ji.id, states.STATE_RUNNING)


def succeeded(ji, data=None):
    """ Notification that the router considers the job succeeded """
    # Notification that the job has finished
    ji.mark_state(states.STATE_EXITED, data)
    tasks.process_dependencies.delay(ji.id, states.STATE_EXITED)

    # Then that we'e succeeded
    ji.mark_state(states.STATE_SUCCEEDED)
    tasks.process_dependencies.delay(ji.id, states.STATE_SUCCEEDED)


def failed(ji, data=None):
    """ Notification that the router considers the job failed """
    # Notification that the job has finished
    ji.mark_state(states.STATE_EXITED, data)
    tasks.process_dependencies.delay(ji.id, states.STATE_EXITED)

    # Then that we'e failed
    ji.mark_state(states.STATE_FAILED)
    tasks.process_dependencies.delay(ji.id, states.STATE_FAILED)


def error(ji, data=None):
    """ Notification that the router has errored, go straight to failed """
    ji.mark_state(states.STATE_FAILED, data)
    tasks.process_dependencies.delay(ji.id, states.STATE_FAILED)


def progress(ji, data=None):
    """ Progress update, not sure what to do with these yet """
    pass


def message(ji, data=None):
    """ Generic message from the router, just mark an event """
    ji.mark_msg(data)
