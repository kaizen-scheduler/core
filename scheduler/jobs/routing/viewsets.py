import logging
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from scheduler.jobs.models import JobInstance
import callbacks

log = logging.getLogger(__name__)


class RoutingAPIMixin(object):
    """ This is a mixin for a django-rest-framework viewset that implements
    the router callback API:

        instances/{jid}/running/
        instances/{jid}/succeeded/
        instances/{jid}/failed/
        instances/{jid}/error/
        instances/{jid}/progress/
        instances/{jid}/message/

    TODO: verify the contents of the request data
    """
    @detail_route(methods=['post'])
    def running(self, request, pk=None):
        return self._callback(callbacks.running, pk, request.data)

    @detail_route(methods=['post'])
    def succeeded(self, request, pk=None):
        return self._callback(callbacks.succeeded, pk, request.data)

    @detail_route(methods=['post'])
    def failed(self, request, pk=None):
        return self._callback(callbacks.failed, pk, request.data)

    @detail_route(methods=['post'])
    def error(self, request, pk=None):
        return self._callback(callbacks.error, pk, request.data)

    @detail_route(methods=['post'])
    def progress(self, request, pk=None):
        return self._callback(callbacks.progress, pk, request.data)

    @detail_route(methods=['post'])
    def message(self, request, pk=None):
        return self._callback(callbacks.message, pk, request.data)

    def _callback(self, func, jid, data):
        ji = JobInstance.objects.get(id=jid)
        log.info("Got router callback %s for %s: %s", func, ji, data)
        return Response(func(ji, data=data))
