from rest_framework import serializers

from scheduler.jobs import models


class JobSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.Job
        read_only_fields = ('name', 'repo')


class JobInstanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = models.JobInstance
        fields = '__all__'
