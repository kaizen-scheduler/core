import logging
from functools import wraps

from celery import shared_task

import dependencies
import models
import states
import timings
from scheduler.routers.config import get_router

log = logging.getLogger(__name__)

TASK_TYPES = {
    'job_control': ['kill', 'launch', 'load'],
    'router_msg': ['running', 'succeeded', 'failed', 'message', 'error', 'progress'],
    'misc': ['timing-satisfied', 'process-dependencies']
}


def log_exceptions(func):
    # TODO: Implement this globally:
    # http://stackoverflow.com/questions/17979655/how-to-implement-autoretry-for-celery-tasks

    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            return func(*args, **kwargs)
        except:
            log.exception("Task failed %s (%s, %s)", func, args, kwargs)
            raise
    return wrapper


@shared_task
@log_exceptions
def process_dependencies(jid, state):
    """ Since the jid has just entered the given state we need
    to check if any other jis depend on that

    # TODO: chain this call into a maybe_launch task
    """
    ji = models.JobInstance.objects.get(id=jid)
    # Mark the ji as satisfied for that state. This returns a list of
    # dependents that may now be fully satisfied (if all their other dependencies
    # are also satisfied)
    maybe_satisfied = dependencies.satisfy(ji, state=state)
    log.debug("Dependencies maybe satisfied by %s in state %s are %s", ji, state, maybe_satisfied)

    # For each one we check to see if theyre fully satisfied and launch if so
    for ji in maybe_satisfied:
        maybe_launch.delay(ji.id)


@shared_task
@log_exceptions
def maybe_launch(jid):
    ji = models.JobInstance.objects.get(id=jid)
    if timings.is_satisfied(ji) and dependencies.is_satisfied(ji):
        # Launch synchronously wihtout spawning a new task
        launch.apply(args=(jid,))


@shared_task(default_retry_delay=5, max_retries=3, bind=True)
@log_exceptions
def launch(self, jid):
    """ Launch the given job instance ID without questioning dependencies """

    # On failure to launch we should make the whole job failed
    def on_failure(exc, task_id, args, kwargs, einfo):
        ji.mark_state(states.STATE_FAILED, data=str(exc))
        process_dependencies.delay(jid, states.STATE_FAILED)
    self.on_failure = on_failure

    ji = models.JobInstance.objects.get(id=jid)

    # TODO: would perhaps be cool to have a decorator that converts jid -> ji and checks valid states?
    if ji.state != states.STATE_LOADED:
        log.warning("Cannot launch %s when %s, should be %s", ji, ji.state, states.STATE_LOADED)
        return

    try:
        get_router(ji.resolved).launch(ji)
        ji.mark_state(states.STATE_LAUNCHED)
        process_dependencies.delay(jid, states.STATE_LAUNCHED)
    except Exception as e:
        # Failed to launch, we'll retry
        launch.retry(exc=e)


@shared_task
@log_exceptions
def kill(jid):
    ji = models.JobInstance.objects.get(id=jid)
    log.info("Killing job instance %s", ji)
    # TODO: should we mark state before/after we send message to the router?
    # Probably after since we don't care about duplicate kills
    ji.mark_state(states.STATE_KILLED)
    get_router(ji.resolved).kill(ji)
    # TODO: Again, we could let people depend on KILLED state but it's a bit weird
    # process_dependencies.delay(jid, states.STATE_KILLED)
