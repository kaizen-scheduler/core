var logContainer = $('#log');

function updateLogs() {
    $.get("{% url 'jobinstance-logs' ji.id %}")
        .done(function(logs) {
            logContainer.text(logs);
        }).fail(function(error) {
            if (error.status === 404) {
                console.log("No logs yet");
            } else {
                // FIXME: whaaa
                console.log("ERROR", error);
            }
        });
}

updateLogs();
window.setInterval(updateLogs, 5000);
