import json

from django import template

register = template.Library()


@register.filter(name='pretty_definition')
def pretty_definition(value):
    # Make sure we have a dictionary
    assert isinstance(value, dict)

    # Then json print it with indents and fancy things
    return json.dumps(value, ensure_ascii=True, indent=4)
