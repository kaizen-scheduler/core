import json
import logging

from scheduler import timings
from scheduler.rungroups.timings import Timings

log = logging.getLogger(__name__)


def is_satisfied(ji):
    """ Check if the JobInstance's time dependencies are satisfied to
    be launched (start_after in the past).

    XXX: This will get more complicated as we need to check other things e.g.
    making sure we're before our kill_after time etc etc """
    return Timings(ji.resolved).past_start_after()


def register(ji):
    """ Register the time dependencies for a given JobInstance
    FIXME: support registration of multiple timings """
    ji_timings = Timings(ji.resolved).timings
    log.info("Registering timings %s for %s", ji_timings, ji)
    for key, time in ji_timings.iteritems():
        msg = {"type": key, "jid": ji.id}
        timings.register(time, json.dumps(msg))
