from django.conf.urls import url

import views

app_name = 'jobs'
urlpatterns = [
    url(r'^jobs/$', views.JobList.as_view(), name='job_list'),
    url(r'^jobs/(?P<pk>[0-9]+)/$', views.JobDetail.as_view(), name='job_detail'),
    url(r'^instances/$', views.JobInstanceList.as_view(), name='instance_list'),
    url(r'^instances/(?P<pk>[0-9]+)/$', views.JobInstanceDetail.as_view(), name='instance_detail'),
]
