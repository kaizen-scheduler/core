from django.core.exceptions import PermissionDenied
from django.views import generic

from scheduler.filetree.mixins import ReadPermissionMixin
from scheduler.filetree.permissions import filter_readable
from filters import JobInstanceFilter
from forms import JobInstanceSearchForm
from models import Job, JobInstance


class JobList(generic.ListView):
    model = Job

    def get_queryset(self):
        qs = super(JobList, self).get_queryset()
        return qs.readable(user=self.request.user)


class JobDetail(ReadPermissionMixin, generic.DetailView):
    model = Job

    def next_definition_diff(self):
        old = self.object.instances.latest().definition
        new = self.object.next_definition
        return old.diff_to(new)

    def get_context_data(self, **kwargs):
        ctx = super(JobDetail, self).get_context_data(**kwargs)
        ctx['definition_diff'] = self.next_definition_diff()
        return ctx


class JobInstanceList(generic.edit.FormMixin, generic.ListView):
    model = JobInstance
    form_class = JobInstanceSearchForm

    def get_form_kwargs(self):
        kwargs = super(JobInstanceList, self).get_form_kwargs()
        kwargs['data'] = self.request.GET
        return kwargs

    def get_queryset(self):
        qs = super(JobInstanceList, self).get_queryset()
        qs = qs.filter(instance=0)
        qs = filter_readable(qs, user=self.request.user, node_field='job')
        return JobInstanceFilter(self.request.GET, queryset=qs).qs


class JobInstanceDetail(generic.DetailView):
    model = JobInstance
    context_object_name = "ji"

    def get_object(self, *args, **kwargs):
        """ Verify object permissions """
        ji = super(JobInstanceDetail, self).get_object(*args, **kwargs)
        if not ji.job.is_readable(user=self.request.user):
            raise PermissionDenied()
        return ji
