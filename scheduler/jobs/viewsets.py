from django.db.models import Count
from rest_framework import viewsets
from rest_framework.decorators import list_route
from rest_framework.response import Response

import filters
from loggers.viewsets import LoggingAPIMixin
import models
from routing.viewsets import RoutingAPIMixin
import serializers


class JobViewSet(viewsets.ReadOnlyModelViewSet):
    queryset = models.Job.objects.all()
    serializer_class = serializers.JobSerializer


class JobInstanceViewSet(LoggingAPIMixin, RoutingAPIMixin, viewsets.ReadOnlyModelViewSet):
    queryset = models.JobInstance.objects.all()
    serializer_class = serializers.JobInstanceSerializer
    filter_class = filters.JobInstanceFilter

    @list_route(methods=['get'])
    def states(self, request):
        """ Return a dictionary of state counts for the current queryset """
        queryset = self.filter_queryset(self.get_queryset())
        states_list = queryset.values_list('state').annotate(Count('state'))
        return Response(dict(states_list))
