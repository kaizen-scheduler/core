""" A temporary (ha!) thread to run a webserver for the API """
import threading
import time

from django.conf import settings
from django.core.management import call_command


class SchedulerWebServer(threading.Thread):

    def run(self):
        server_conf = settings.CONFIG['server']
        address = "%s:%s" % (server_conf['hostname'], server_conf['port'])
        while True:
            try:
                call_command('runserver', address, '--noreload')
            except Exception:
                pass
            finally:
                time.sleep(5)
