from __future__ import absolute_import

import multiprocessing

from celery import current_app
from celery.bin import worker
from django.conf import settings


class CeleryWorker(multiprocessing.Process):

    def run(self):
        app = current_app._get_current_object()
        w = worker.worker(app=app)
        w.run(**settings.CONFIG['celery-worker'])
