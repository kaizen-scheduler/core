import sys


class WriteLoggingMixin(object):

    def write(self, ji, fd=sys.stdout.fileno(), offset=None):
        raise NotImplementedError()


class BaseLoggingBackend(object):
    """ Base implementation of a Kaizen logging backend """

    def preload(self, ctx):
        """ Optionally perform preloading steps for the logging config """
        return ctx

    def read(self, ji, fd=sys.stdout.fileno(), offset=0):
        raise NotImplementedError()

    def is_writable(self):
        # TODO: this is a bit weak
        return hasattr(self, 'write')


class ReadWriteLoggingBackend(WriteLoggingMixin, BaseLoggingBackend):
    pass
