from django.conf import settings


class LoggerConfig():
    """ A wrapper for logger configuration """

    def __init__(self):
        conf = settings.CONFIG['job-logging']
        self.default_logger = conf['default-logger']
        self.loggers = {
            name: _instantiate_logger(dict(logger_conf))
            for name, logger_conf in conf['loggers'].iteritems()
        }
        assert self.default_logger in self.loggers

    def get_logger(self, name):
        return self.loggers[name]


def _instantiate_logger(logger_conf):
    clazz = logger_conf.pop('class')
    mod, cls = clazz.rsplit(".", 1)
    logger_cls = getattr(__import__(mod, fromlist=[cls]), cls)
    return logger_cls(logger_conf)
