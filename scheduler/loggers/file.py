import os
import sys
from base import BaseLoggingBackend


class FileLoggingBackend(BaseLoggingBackend):
    """ A file based implementation of a logging backend """

    def __init__(self, config):
        self.base_path = config['base-path']

    def preload(self, ctx):
        """ Add the logging configuration to the definition """
        return ctx

    def read(self, ji, **kwargs):
        # FIXME: handle offsets
        with open(self._path_for_ji(ji), 'r') as f:
            return f.read()

    def write(self, ji, lines, **kwargs):
        with open(self._path_for_ji(ji), 'a+') as f:
            f.writelines(lines)

    def _path_for_ji(self, ji, fd=sys.stdout.fileno()):
        return os.path.join(self.base_path, ji.job.path.replace("/", "_") + "." + str(fd))
