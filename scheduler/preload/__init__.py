import pytz


class PreloadContext(object):
    """
    A PreloadContext is the object that is passed to each of the preloader
    plugins to transform raw definitions into compliant JSON objects
    """
    def __init__(self, job, definition, rungroup, warnings, errors):
        self.job = job
        self.definition = definition
        self.rungroup = rungroup
        self.timezone = pytz.utc

        self.warnings = warnings
        self.errors = errors

    def warning(self, plugin, msg):
        self.warnings.append((plugin, msg))

    def error(self, plugin, msg):
        self.errors.append((plugin, msg))

    def gettime(self, datetime):
        # Add UTC offset for our timezone converting everything to UTC
        return datetime + self.timezone.utcoffset(datetime)
