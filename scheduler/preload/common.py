""" Some common preloaders """
import logging
import pytz

log = logging.getLogger(__name__)


def logging(ctx):
    """ Configure default logging for stdout and stderr """
    logging = ctx.definition.get('logging', {})

    for fd in ["stdout", "stderr"]:
        if fd not in logging:
            logging.setdefault(fd, {
                "collector": "ksr.collectors.file.FileLogCollector",
                "parameters": {"path": "/tmp/" + ctx.job.path.replace("/", "_") + "." + fd[3:]}
            })

    ctx.definition['logging'] = logging
    return ctx


def rungroup(ctx):
    """ Make sure the rungroup exists in the definition """
    ctx.definition['rungroup'] = str(ctx.rungroup)
    return ctx


def timezone(ctx):
    """ Set the timezone if one exists """
    if ctx.definition.get('timezone'):
        ctx.timezone = pytz.timezone(ctx.definition['timezone'])
    return ctx
