"""
The dependencies preloader normalises any dependencies into the type:

    [{Name: State,...},...]

This represents the dependencies of a process in CNF.
There is currently no expression simplification performed
e.g:
    [{A: S, B: S}, {B: S, C:S}] means (A v B) ^ (B v C)

    # FIXME: docs are out of date

Each element is a dictionary (since tuples are not serializable in JSON) that
represents the state of the dependency that we are interested in. This defaults
to SUCCEEDED

Dependencies in the raw process definition can be described in a number of ways:
(where S is short for SUCCEEDED)
    * A => A == A:S == [A] == [[A]] == [[{A: S}]]
    * A ^ B => [A, B] == [[A], [B]] == [[{A: S}], [{B: S}]]
    * A v B => [[A, B]] == [[{A: S}, {B: S}]]

Note the reason we do not collapse the list of dicts into a single dict is
that the key may not be unique. It is perfectly reasonable to depend on process
A SUCCEEDED OR FAILED in the same statement
"""
import logging

from scheduler.jobs.models import Job

log = logging.getLogger(__name__)


def normalise(ctx):
    """ Normalise the dependencies.
    Depends on rungroup """
    ctx.definition['dependencies'] = _normalise_dependencies(ctx.definition.get('dependencies', []), ctx.rungroup)
    return ctx


def _normalise_dependencies(deps, rungroup):
    log.debug("Normalising dependencies: %s", deps)

    path_map = _get_path_job_id_map(deps)

    new_deps = []
    for and_idx in deps:
        if not isinstance(and_idx, dict):
            raise ValueError("We should have a dict here?!")

        new_deps.append({
            "%d:%s" % (path_map[path], rungroup): state for path, state in and_idx.iteritems()
        })

    return new_deps


def _get_path_job_id_map(deps):
    paths = set()
    for and_idx in deps:
        for path in and_idx:
            paths.add(path)
    return dict(Job.objects.filter(path__in=paths).values_list('path', 'id'))
