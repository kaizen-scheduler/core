""" Resolve the timings """
import logging

import isodate

log = logging.getLogger(__name__)


FIELDS = {
    'start_after': 0
}


def resolve(ctx):
    """ Resolve the relative timing definitions into absolute ones """
    # Get the definition, defaulting to empty def
    timings_def = ctx.definition.get('timings', {})

    # Only keep the parsed ones
    timings = {}
    for field, offset_multiplier in FIELDS.iteritems():
        dt = _resolve_field(
            timings_def.get(field),
            ctx.rungroup, offset_multiplier
        )
        timings[field] = ctx.gettime(dt).isoformat()

    ctx.definition['timings'] = timings
    return ctx


def _resolve_field(field, rungroup, default_offset_multiplier):
    """
    Resolve the field value given the start of the rungroup, the duration of
    the rungroup, and the default offset multipler (number of rungroups)
    """
    if field is None:
        # Use the default offset multiplier
        return rungroup.starts_at + (rungroup.period.duration * default_offset_multiplier)

    # Else parse the field as ISO8601 duration

    # We allow whitespace for readability, strip it now (space, tab, newline(?!) etc)
    field = "".join(field.split())

    delta = isodate.parse_duration(field)

    # TODO: validate how big deltas can be? Any ideas?
    return rungroup.starts_at + delta
