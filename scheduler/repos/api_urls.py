from rest_framework_extensions import routers
from viewsets import RepoViewSet, SyncTaskViewSet


def register(name, router):
    repos_router = router.register(name, RepoViewSet, base_name='repos')
    repos_router.register('syncs', SyncTaskViewSet, base_name='repos-sync', parents_query_lookups=['repo_id'])


# In case we want to be included somewhere
router = routers.ExtendedDefaultRouter()
urlpatterns = register('repos', router)
