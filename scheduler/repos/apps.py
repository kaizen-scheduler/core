from django.apps import AppConfig


class ReposConfig(AppConfig):
    name = 'scheduler.repos'

    def ready(self):
        from scheduler.repos import handlers  # noqa
