from django import forms

from models import Repo


class RepoForm(forms.ModelForm):

    def clean_path(self):
        """ Make sure the path ends with a slash """
        path = self.cleaned_data['path']
        return path if path.endswith("/") else path + "/"

    class Meta:
        model = Repo
        fields = ['path', 'description', 'url', 'commitish', 'root', 'group']
        labels = {
            'path': 'Mount Path'
        }
