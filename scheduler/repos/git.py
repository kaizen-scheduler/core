""" Wrapper around a GitRepo """
import logging
import os
import re
import shutil
import subprocess

from django.conf import settings

log = logging.getLogger(__name__)

GIT_MIN_VERSION = (2, 3, 0)


class GitRepo(object):

    def __init__(self, working_dir, url, commitish, root="/"):
        self.working_dir = working_dir
        self.url = url
        self.commitish = commitish
        # Normalise root so it doesn't start with /, but does end with one
        root = root[1:] if root.startswith("/") else root
        root = root + '/' if not root.endswith("/") else root
        self.root = os.path.join(root, "")

    def is_repo(self):
        """ Return True if the working directory is a git repo """
        try:
            # Returns the path to the .git directory (but since all our
            # git commands are relative to the working_dir, we want this to
            # be '.git'
            git_dir = self._exe(['rev-parse', '--git-dir'])
            return git_dir == '.git'
        except subprocess.CalledProcessError:
            log.warn("Directory %s is not a git repository", self.working_dir)
            return False

    def commit_hash(self):
        """ Return the current commit_hash """
        return self._exe(['rev-parse', '--verify', 'HEAD'])

    def diff(self, ref1, ref2):
        """ Diff two refs and return a map of added/modified/deleted (A/M/D) files """
        diffs = [d.split() for d in self._exe(['diff', '--name-status', '--no-renames', ref1, ref2]).splitlines()]

        # Filter out anything not within the root
        diffs = filter(lambda diff: diff[1].startswith(self.root), diffs)

        return {
            'A': [d[1][len(self.root):] for d in filter(lambda d: d[0] == 'A', diffs)],
            'M': [d[1][len(self.root):] for d in filter(lambda d: d[0] == 'M', diffs)],
            'D': [d[1][len(self.root):] for d in filter(lambda d: d[0] == 'D', diffs)],
        }

    def file_at_commit(self, filepath, commitish="HEAD"):
        """ Get the contents of a file at the given commitish """
        path = os.path.join(self.root, filepath)
        # Get the latest commit on the file before the given commitish
        latest_hash = self._exe(['log', '--pretty=oneline', '-n', '1', commitish, '--', path]).split()[0]
        return (latest_hash, self._exe(['show', '%s:%s' % (latest_hash, path)]))

    def list_files(self):
        """ Return a list of files in the repo """
        files = self._exe(['ls-tree', '--full-tree', '--name-only', '-r', 'HEAD']).splitlines()
        return [f[len(self.root):] for f in files if f.startswith(self.root)]

    def update(self, clean=False):
        """ Based on the article here:
        https://grimoire.ca/git/stop-using-git-pull-to-deploy
        we use the following steps to keep our repo in sync with the remote.

        cd "${DEPLOY_TREE}"
        git fetch --all
        git checkout --force "${TARGET}"
        git submodule sync
        git submodule update --init --recursive
        """
        if clean or not self.is_repo():
            log.debug("Cleaning git repository cache")
            shutil.rmtree(self.working_dir, ignore_errors=True)
            os.makedirs(self.working_dir)
            log.debug("Cloning git repo from %s", self.url)
            self._exe(['clone', self.url, self.working_dir])

        assert self.is_repo()

        log.info("Checking out commitish %s", self.commitish)
        try:
            self._exe(['pull', '--all', '--force'])
            self._exe(['checkout', '--force', self.commitish])
            self._exe(['submodule', 'sync'])
            self._exe(['submodule', 'update', '--init', '--recursive'])
        except subprocess.CalledProcessError:
            log.exception("Failed to checkout commitish, blowing away cache")
            shutil.rmtree(self.working_dir, ignore_errors=True)

    def _exe(self, args):
        """ Execute a git command, if specified also use the given ssh key """
        key = settings.CONFIG['repos'].get('ssh_private_key')
        if key:
            os.environ['GIT_SSH_COMMAND'] = "ssh -i %s" % key

        cmd = [GIT, '-C', self.working_dir, '-c', 'color.ui=never'] + args
        return subprocess.check_output(cmd).strip()


def _git_exe():
    """ Little test to see if git exists and it's a high enough version """
    try:
        git = subprocess.check_output(['/bin/bash', '-c', 'which git']).strip()
        git_version = subprocess.check_output([git, '--version'])

        # Grab the version string of the form "git version x.x.x"
        version_str = re.search("git version (?P<version>.*)$", git_version).group('version')
        version = tuple([int(i) for i in version_str.strip().split(".")])

        # Check the version
        if version < GIT_MIN_VERSION:
            raise Exception("Git version %s not high enough, need %s" % (str(version), str(GIT_MIN_VERSION)))

        return git
    except subprocess.CalledProcessError:
        raise Exception("Cannot find 'git' on the path")


# Grab the git executable, should use GitPython in future
GIT = _git_exe()
