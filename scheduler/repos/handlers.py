import logging

from django.db.models.signals import post_save
from django.dispatch import receiver

from models import Repo
from scheduler.signals import do_recovery
from tasks import sync_repo, sync_repos

log = logging.getLogger(__name__)


@receiver(do_recovery)
def on_recovery(sender, startup=None, **kwargs):
    """ For us this means updating the repos.
    If we're starting up, do a force update """
    log.info("Updating all repos force=%s", bool(startup))
    repo_ids = Repo.objects.values_list('id', flat=True)
    sync_repos(repo_ids, force=startup)


@receiver(post_save, sender=Repo)
def on_repo_save(sender, instance=None, **kwargs):
    sync_repo(instance.id)
