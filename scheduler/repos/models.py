import hashlib
import os

from django.conf import settings
from django.db import models

from django_celery_results.models import TaskResult
from git import GitRepo
from scheduler.filetree.models import Directory, NodeQuerySet


class FilePathField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 256)
        super(FilePathField, self).__init__(*args, **kwargs)


class CommitHashField(models.CharField):
    def __init__(self, *args, **kwargs):
        kwargs.setdefault('max_length', 40)
        super(CommitHashField, self).__init__(*args, **kwargs)


class RepoSyncTask(models.Model):
    """ A model to keep track of a repo sync task """
    repo = models.ForeignKey('Repo', related_name='sync_tasks')
    task = models.ForeignKey(TaskResult, to_field='task_id', db_constraint=False)
    created_at = models.DateTimeField(auto_now_add=True)

    @property
    def result(self):
        import tasks
        return tasks.sync_repo_result(self.task_id)

    class Meta:
        get_latest_by = 'created_at'


class Repo(Directory):
    """ A repo is a directory node representing a git repository """
    # Description for the repository
    description = models.CharField(max_length=256)

    # URL for how to checkout the repo
    url = models.CharField(max_length=256)

    # Root directory we need to pay attention to
    root = models.CharField(max_length=256)

    # The commitish to checkout (branch, tag or commit)
    commitish = models.CharField(max_length=256, default="master")

    # Last known 'good' commit_hash
    commit_hash = models.CharField(max_length=40, null=True)

    # TODO: will need some more fields to store authentication params
    # For example, need a secret key to POST to the /repo/{id}/sync URL

    objects = NodeQuerySet.as_manager()

    def get_cache_dir(self):
        """ Returns the local cache directory for the given repository.
        The key to use here is the id+url since we allow the commitish
        to change.
        """
        cache_dir = settings.CONFIG['repos']['cache_dir']
        repo_hash = hashlib.sha1(str(self.id) + self.url).hexdigest()
        return os.path.join(cache_dir, repo_hash)

    def get_git_repo(self):
        return GitRepo(self.get_cache_dir(), self.url, self.commitish, self.root)

    def icon(self):
        return "database"

    def __unicode__(self):
        return self.path
