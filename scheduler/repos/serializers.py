from django_celery_results.models import TaskResult
from expander import ExpanderSerializerMixin
from rest_framework import serializers

from models import Repo, RepoSyncTask


class TaskResultSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskResult
        exclude = ('id',)


class RepoSerializer(ExpanderSerializerMixin, serializers.ModelSerializer):

    job_count = serializers.ReadOnlyField(source='jobs.count')

    class Meta:
        model = Repo
        # TODO: set default commitish to model default? Shouldn't that happen already?
        read_only_fields = ('commit_hash',)


class SyncTaskSerializer(serializers.ModelSerializer):

    task = TaskResultSerializer(read_only=True)

    class Meta:
        model = RepoSyncTask
