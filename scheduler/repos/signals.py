import django.dispatch

# Sent whenever a repository is updated
repo_updated = django.dispatch.Signal(providing_args=["repo", "commit_hash", "diffs"])
