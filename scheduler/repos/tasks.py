import logging

from celery import shared_task

from git import GitRepo
from models import Repo, RepoSyncTask
from signals import repo_updated

log = logging.getLogger(__name__)


def sync_repo_result(task_id):
    return _sync_repo.AsyncResult(task_id)


def sync_repos(repo_ids, force=False):
    """ Create a set of tasks to sync the repos and wait for them all to finish """
    return [sync_repo(repo_id, force=force) for repo_id in repo_ids]


def sync_repo(repo_id, force=False):
    task = _sync_repo.si(repo_id, force=force)
    task.freeze()
    result = task.delay()
    RepoSyncTask.objects.create(repo_id=repo_id, task_id=task.id)
    return result


@shared_task(bind=True)
def _sync_repo(self, repo_id, force=False):
    r = Repo.objects.get(id=repo_id)
    log.info("Synchronizing repo: %s", r)

    repo = GitRepo(r.get_cache_dir(), r.url, r.commitish, r.root)
    repo.update()
    new_commit_hash = repo.commit_hash()

    if not r.commit_hash or force:
        # Commit hash has never been set, all files are new
        # FIXME: oh this is ugly...
        log.info("Running full sync on repo %s", r)
        diffs = {'A': repo.list_files(), 'M': [], 'D': []}
    else:
        log.info("Fetching diffs from %s to %s", r.commit_hash, new_commit_hash)
        diffs = repo.diff(r.commit_hash, new_commit_hash)

    log.info("Got diffs: %s", diffs)

    if not any([len(v) > 0 for v in diffs.values()]):
        # Exit early if there are no diffs
        return diffs

    # Returns a list of responses from signal handlers  [(receiver, response), ... ]
    resps = repo_updated.send_robust(
        sender=self.request.id, repo=r,
        commit_hash=new_commit_hash, diffs=diffs
    )

    log.debug("Got responses from %d repo_updated signal handlers", len(resps))

    # Any bad ones we can log their exception objects. Though to get a stack trace
    # the handler itself needs to log.exception
    bad_resps = filter(lambda resp: isinstance(resp[1], Exception), resps)
    for resp in bad_resps:
        log.error("Signal handler failed with: %s", resp)

    # If no reponses are bad (thrown), then we can update the commit_hash of the repo
    # This requires all signal handlers to be idempotent as any failure causes the diffs to be resent
    if len(bad_resps) > 0:
        log.error("One or more signal handlers failed, not updating commit hash for repo %s: %s", r, bad_resps)
        # Meh, for now just raise the first one. They all need to be fixed anyway!
        raise bad_resps[0][1]

    log.debug("All signal handlers succeeded, updating commit hash of repo %s to %s", r, new_commit_hash)
    r.commit_hash = new_commit_hash
    r.save()
    return diffs
