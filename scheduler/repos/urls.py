from django.conf.urls import url

import views

app_name = 'repos'
urlpatterns = [
    url(r'^$', views.RepoList.as_view(), name='list'),
    url(r'^create/$', views.RepoCreate.as_view(), name='create'),
    url(r'^(?P<pk>[0-9]+)/$', views.RepoDetail.as_view(), name='detail'),
    url(r'^(?P<pk>[0-9]+)/delete/$', views.RepoDelete.as_view(), name='delete'),
    url(r'^(?P<pk>[0-9]+)/edit/$', views.RepoEdit.as_view(), name='edit'),
    url(r'^(?P<pk>[0-9]+)/sync/$', views.RepoSync.as_view(), name='sync'),
]
