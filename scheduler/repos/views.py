from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.views import generic
from scheduler.filetree.mixins import ReadPermissionMixin, ExecutePermissionMixin

from forms import RepoForm
from models import Repo
import tasks


class RepoList(generic.ListView):
    model = Repo

    def get_queryset(self):
        """ We filter out any repos we don't have read permission on.
        This is arguably incorrect and we may wish to change this in future to
        match unix semantics. That is, to see the existence of a repo we only need
        read permissions on its parent directory """
        qs = super(RepoList, self).get_queryset()
        return qs.readable(user=self.request.user)


class RepoDetail(ReadPermissionMixin, generic.DetailView):
    model = Repo


class RepoSync(ExecutePermissionMixin, generic.DetailView):
    model = Repo

    def get(self, request, *args, **kwargs):
        repo = self.get_object()
        tasks.sync_repo(repo.id)
        return HttpResponseRedirect(reverse('repos:detail', args=(repo.id,)))


class RepoCreate(generic.CreateView):
    model = Repo
    form_class = RepoForm
    success_url = '/repos/{id}'

    def form_valid(self, form):
        """ Save the form setting the owner to the current user """
        self.object = form.save(commit=False)
        self.object.user = self.request.user
        self.object.save(mkdirs=True)
        return HttpResponseRedirect(self.get_success_url())


class RepoDelete(generic.DeleteView):
    model = Repo

    def get_success_url(self):
        return '/repos/%d' % self.object.id


class RepoEdit(generic.UpdateView):
    model = Repo
    form_class = RepoForm
    template_name = "repos/repo_edit.html"
