import logging

from rest_framework import decorators, status, viewsets
from rest_framework.response import Response
from rest_framework_extensions.mixins import NestedViewSetMixin

import tasks
from models import Repo, RepoSyncTask
from serializers import RepoSerializer, SyncTaskSerializer

log = logging.getLogger(__name__)


class RepoViewSet(viewsets.ModelViewSet):
    queryset = Repo.objects.all()
    serializer_class = RepoSerializer

    @decorators.detail_route(methods=['GET'])
    def sync(self, request, pk=None):
        """ Synchronize our local cache with the remote repository.

        FIXME: this should be a POST method with appropriate authentication.
        Token auth can ONLY be used over HTTPS

        TODO: rate-limit this view

        TODO: how do we handle permissions here? A secret key in the POST
        body? Or make the caller log in?
        """
        result = tasks.sync_repo(pk, force=request.query_params.get('force'))
        return Response(result.get(), status=status.HTTP_200_OK)


class SyncTaskViewSet(NestedViewSetMixin, viewsets.ModelViewSet):
    queryset = RepoSyncTask.objects.all()
    serializer_class = SyncTaskSerializer

    @decorators.list_route(methods=['GET'])
    def latest(self, request, *args, **kwargs):
        task = self.get_queryset().latest()
        serializer = self.get_serializer(task)
        return Response(serializer.data)
