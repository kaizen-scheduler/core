from config import get_router

default_app_config = 'scheduler.routers.apps.RoutersConfig'


def preload(ctx):
    """ The logging preloader runs the preloader for the job's logging backend.
    FIXME: change to prelaunch """
    router = get_router(ctx.definition)
    return router.preload(ctx)
