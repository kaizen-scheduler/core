from django.apps import AppConfig


class RoutersConfig(AppConfig):
    name = 'scheduler.routers'

    def ready(self):
        from scheduler.routers import handlers  # noqa
