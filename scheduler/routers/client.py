""" Library for accessing routers """
import logging
import os
import requests
from scheduler.loggers.config import LoggerConfig

log = logging.getLogger(__name__)


class BaseRouter(object):
    """ Interface for talking to routers.

    TODO: add a "release" message """

    def __init__(self, name, config, callback_url):
        self.name = name
        self.callback_url = callback_url
        self.config = config

    def preload(self, ctx):
        """ Optionally run preload functions for the given router """
        return ctx

    def logging_backend(self, definition):
        """ Returns the logger backend this definition should use """
        raise NotImplementedError()

    def refresh(self):
        """ The router must resend messages for any known jobs """
        raise NotImplementedError()

    def launch(self, ji):
        raise NotImplementedError()

    def kill(self, ji):
        raise NotImplementedError()


class HTTPRouter(BaseRouter):
    """ Version 1 implementation of the HTTP Routing API. """

    def __init__(self, *args, **kwargs):
        super(HTTPRouter, self).__init__(*args, **kwargs)
        self.url = self.config['url']

    def refresh(self):
        """ Send a refresh message to the router """
        log.info("Refreshing job states for router %s", self.name)
        response = requests.post(os.path.join(self.url, 'refresh/'))
        response.raise_for_status()

    def launch(self, ji):
        """ Send a launch message to the router """
        msg = {
            'jid': ji.id,
            'name': ji.definition.name,
            'rungroup': ji.rungroup,
            'callback_url': self.callback_url,
            'definition': ji.resolved
        }
        log.info("Launching job %s via router %s", ji, self.name)

        response = requests.post(self.url, json=msg)
        log.debug("Router %s replied: %s", self.name, response.content)
        with open('/tmp/router.html', 'w+') as f:
            f.write(response.content)
        response.raise_for_status()

        # FIXME: We should behave better if the ID doesn't exist in the response
        # For example, we could just assume the jid instead? Better than failing
        # and being relaunched
        ji.router_id = str(response.json()['id'])
        ji.save()

    def kill(self, ji):
        """ Send a kill message to the router """
        msg = {'jid': ji.id}
        url = os.path.join(self.url, str(ji.router_id), 'kill/')
        log.info("Killing job %s via router %s @ %s", ji, self.name, url)
        response = requests.post(url, json=msg)
        log.debug("Router %s replied: %s", self.name, response.content)
        with open('/tmp/router.html', 'w+') as f:
            f.write(response.content)
        response.raise_for_status()


class KaizenSSHRouter(HTTPRouter):
    """ The Kaizen SSH router accepts jobs over HTTP and uses the FileLoggingBackend """

    def preload(self, ctx):
        logging = ctx.definition.get('logging', {})

        # FIXME: this should be the full URL (including jid) and also not be hard coded...
        logging = {
            'collector': {
                'class': 'ksr.collectors.http.HTTPLogCollector',
                'url': 'http://localhost:7777/api/v1/instances/20/logs/',
            },
            'handles': ['stdout', 'stderr']
        }

        ctx.definition['logging'] = logging
        return ctx

    def logging_backend(self, definition):
        """ Return the name of the logger backend we want the job the use. For KSR we always
        use the one configured in the config """
        logger = LoggerConfig().get_logger(self.config['logger'])
        # Make sure we have a read/write logger
        assert logger.is_writable()
        return logger
