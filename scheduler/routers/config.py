from django.conf import settings


def get_router(definition):
    """ Get a Router instance for the given definition """
    # TODO: we can cache these instantiated router clients right?
    conf = RoutingConfig()
    router_name = definition.get('router', {}).get('name', conf.default_router)
    return conf.routers[router_name]


class RoutingConfig():
    """ A wrapper for routing configuration """

    def __init__(self):
        conf = settings.CONFIG['routing']
        self.callback_url = conf['callback-url']
        self.default_router = conf['default-router']
        self.routers = {
            name: _instantiate_router(name, dict(router_conf), self.callback_url)
            for name, router_conf in conf['routers'].iteritems()
        }
        assert self.default_router in self.routers

    def get_router(self, name):
        return self.routers[name]


def _instantiate_router(name, router_conf, callback_url):
    clazz = router_conf.pop('class')
    mod, cls = clazz.rsplit(".", 1)
    router_cls = getattr(__import__(mod, fromlist=[cls]), cls)
    return router_cls(name, router_conf, callback_url)
