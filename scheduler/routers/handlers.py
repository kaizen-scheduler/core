import logging
from django.dispatch import receiver

from scheduler.signals import do_recovery
from config import RoutingConfig

log = logging.getLogger(__name__)


@receiver(do_recovery)
def on_recovery(sender, startup=None, **kwargs):
    """ For each router we need to request job statuses and act on them.

    XXX: is this better done by telling the router to resend status updated?
    Or by actively querying for them?
    """
    routers = RoutingConfig().routers
    log.info("Running router recovery for %d routers", len(routers))
    for name, router in routers.iteritems():
        try:
            router.refresh()
        except Exception:
            log.exception("Failed to refresh jobs for %s router", name)
