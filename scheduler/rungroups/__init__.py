def period_from_definition(definition):
    return definition.get('schedule', {}).get('period', 'daily')
