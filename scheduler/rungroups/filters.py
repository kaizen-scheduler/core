from ics import Calendar
from urllib2 import urlopen


class InvalidScheduleFilter(Exception):
    pass


class NotInSchedule(Exception):
    pass


class Filter(object):

    def __init__(self, value):
        self.value = value

    def assert_matches(self, rungroup):
        raise NotImplementedError()


class PeriodFilter(Filter):
    def assert_matches(self, rungroup):
        if self.value != rungroup.period.name:
            raise NotInSchedule("Rungroup period %s doesn't match schedule %s" % (rungroup.period.name, self.value))


class CalendarFilter(Filter):

    def __init__(self, *args, **kwargs):
        super(CalendarFilter, self).__init__(*args, **kwargs)
        self._cal = None
        self.negate = False
        self.url = self.value

        if self.value.startswith("!"):
            self.url = self.value[1:]
            self.negate = True

    def calendar(self):
        if not self._cal:
            # Fetch the calendar
            self._cal = Calendar(urlopen(self.url).read().decode('iso-8859-1'))
        return self._cal

    def assert_matches(self, rungroup):
        def covers(event):
            # TODO: support timezones better
            return event.begin.naive <= rungroup.starts_at and event.end.naive > rungroup.starts_at

        try:
            events = len(filter(covers, self.calendar().events)) > 0
        except Exception as e:
            raise NotInSchedule("Failed to fetch calendar from %s: %s" % (self.url, e))

        #  neg events  raise
        #  y   y       y
        #  y   n       n
        #  n   y       n
        #  n   n       y
        if not (self.negate ^ events):
            raise NotInSchedule("Rungroup %s %scovered by events from %s" % (
                rungroup, "" if self.negate else "not ", self.url)
            )


class DayFilter(Filter):
    DAYS = ["monday", "tuesday", "wednesday", "thursday", "friday", "saturday", "sunday"]

    def assert_matches(self, rungroup):
        options = _parse_options(self.value, self.DAYS)
        if rungroup.starts_at.strftime("%A").lower() not in options:
            raise NotInSchedule("Rungroup %s doesn't match day filter %s" % (rungroup, self.value))


class HourFilter(Filter):
    HOURS = ["%02d" % h for h in range(0, 24)]

    def assert_matches(self, rungroup):
        options = _parse_options(self.value, self.HOURS)
        if rungroup.starts_at.strftime("%H") not in options:
            raise NotInSchedule("Rungroup %s doesn't match hour filter %s" % (rungroup, self.value))


def _parse_options(val, all_options):
    if val is None:
        # Default open
        return all_options

    elif ',' in val:
        # Recursively parse each entry in the comma separated list
        options = []
        for entry in val.split(","):
            options.extend(_parse_options(entry.strip(), all_options))
        return list(set(options))

    elif val.count('-') == 1:
        # We have a range
        start, end = val.split("-")

        if start not in all_options or end not in all_options:
            raise InvalidScheduleFilter(val)

        start_i = all_options.index(start)
        # Add one to make the range inclusive on both ends
        end_i = all_options.index(end) + 1

        if end_i <= start_i:
            # We need to loop the options
            end_i += len(all_options)
            all_options = list(all_options + all_options)

        return all_options[start_i:end_i]

    elif val in all_options:
        return [val]

    raise InvalidScheduleFilter(val)
