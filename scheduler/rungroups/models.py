from collections import namedtuple
from datetime import datetime, timedelta
import re

from django.core.exceptions import ValidationError
from django.db import models


RunPeriod = namedtuple('RunPeriod', ['name', 'short_name', 'regex', 'duration'])


def _re(expr):
    return re.compile(expr.format(**{
        'year': '(?P<year>\d{4})',
        'month': '(?P<month>\d{2})',
        'day': '(?P<day>\d{2})',
        'hour': '(?P<hour>\d{2})',
    }))


START_OF_TIME = datetime.fromtimestamp(0)
END_OF_TIME = datetime.strptime("99991231", "%Y%m%d")
FOREVER = END_OF_TIME - START_OF_TIME

HOURLY = 'hourly'
DAILY = 'daily'
ADHOC = 'adhoc'
SERVICE = 'service'

RUNPERIODS = {
    HOURLY: RunPeriod(HOURLY, 'H', _re('^H{year}{month}{day}:{hour}$'), timedelta(hours=1)),
    DAILY: RunPeriod(DAILY, 'D', _re('^D{year}{month}{day}$'), timedelta(days=1)),
    ADHOC: RunPeriod(ADHOC, 'A', _re('^adhoc$'), FOREVER),
    SERVICE: RunPeriod(SERVICE, 'S', _re('^service$'), FOREVER),
}


class InvalidRungroup(ValueError):
    pass


def validate_rungroup(rungroup_str):
    try:
        RunGroup.from_string(rungroup_str)
    except InvalidRungroup as e:
        raise ValidationError(e)


class RunPeriodField(models.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 10
        kwargs['choices'] = [(p, p) for p in RUNPERIODS.keys()]
        super(RunPeriodField, self).__init__(*args, **kwargs)


class RunGroupField(models.CharField):

    def __init__(self, *args, **kwargs):
        kwargs['max_length'] = 20
        kwargs['validators'] = kwargs.get('validators', [])
        kwargs['validators'].append(validate_rungroup)
        super(RunGroupField, self).__init__(*args, **kwargs)


class RunGroup(models.Model):

    name = RunGroupField(primary_key=True)
    starts_at = models.DateTimeField()
    period = RunPeriodField()

    def __str__(self):
        return self.name

    @classmethod
    def now(cls, period=DAILY):
        return cls.relative(0, period=period)

    @classmethod
    def relative(cls, offset, period=DAILY):
        if period == ADHOC or period == SERVICE:
            return cls.from_string(period)

        period = RUNPERIODS[period]
        now = datetime.utcnow()

        now = now.replace(minute=0, second=0, microsecond=0)
        if period.name == DAILY:
            now = now.replace(hour=0)

        now = now + (offset * period.duration)
        name = _name_from_period_and_start(period.name, now)
        return cls(name=name, period=period, starts_at=now)

    @classmethod
    def from_string(cls, rungroup_str):
        period = _period_from_rungroup_string(rungroup_str)
        starts_at = _starts_at_from_rungroup_string(rungroup_str)
        return cls(name=rungroup_str, period=period, starts_at=starts_at)


def _name_from_period_and_start(period, starts_at):
    # Trivial for adhoc and service
    if period == ADHOC or period == SERVICE:
        return period

    fmt = "{short_name}{year:04d}{month:02d}{day:02d}"
    if period == HOURLY:
        fmt += ":{hour:02d}"

    return fmt.format(
        short_name=RUNPERIODS[period].short_name,
        year=starts_at.year, month=starts_at.month,
        day=starts_at.day, hour=starts_at.hour
    )


def _starts_at_from_rungroup_string(rungroup_str):
    period = _period_from_rungroup_string(rungroup_str)
    m = period.regex.match(rungroup_str)

    if m is None:
        raise InvalidRungroup(rungroup_str)

    if period.name == "adhoc" or period.name == "service":
        return START_OF_TIME

    return datetime(
        year=int(m.group('year')),
        month=int(m.group('month')),
        day=int(m.group('day')),
        hour=int(m.groupdict().get('hour') or 0)
    )


def _period_from_rungroup_string(rungroup_str):
    short_name = rungroup_str[:1].upper()
    for period in RUNPERIODS.values():
        if period.short_name == short_name:
            return period
    raise InvalidRungroup(rungroup_str)
