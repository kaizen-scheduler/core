import logging
import pytz

import filters
from models import DAILY

log = logging.getLogger(__name__)

# TODO: we could make this extensible in the config
FILTERMAP = {
    'calendar': filters.CalendarFilter,
    'day': filters.DayFilter,
    'hour': filters.HourFilter,
}


class Schedule(object):

    def __init__(self, definition):
        self.definition = definition.get('schedule', {})
        self.period = definition.get('period', DAILY)
        self.timezone = pytz.timezone(definition.get('timezone', "UTC"))
        self.filters = _get_filters(definition.get('when', {}), FILTERMAP)

    def matches(self, rungroup):
        try:
            self.assert_matches(rungroup)
            return True
        except filters.NotInSchedule:
            return False

    def assert_matches(self, rungroup):
        for fltr in self.filters:
            fltr.assert_matches(rungroup)


def _get_filters(when_def, filtermap):
    filters = []
    for key, value in when_def.iteritems():
        # FIXME: implement a preloader to verify schedule filters
        if key in filtermap:
            filters.append(filtermap[key](value))
    return filters
