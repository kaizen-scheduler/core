from datetime import datetime
import isodate
from schedule import Schedule


START_AFTER = "start_after"

TIMINGS = [START_AFTER]

# Default timing values based on offset from rungroup start plus
# this many multiples of the rungroup duration
DEFAULT_OFFSET_MULTIPLIERS = {
    # Start after should always default to start of the rungroup
    START_AFTER: 0,
}


class Timings(object):

    def __init__(self, definition):
        # Because of the preloader, we know all of our timings exist
        raw_timings = Schedule(definition).definition['timings']
        self.timings = {
            t: isodate.parse_datetime(raw_timings[t]) for t in raw_timings
        }

    def past_start_after(self):
        return self.timings[START_AFTER] < datetime.utcnow()


def preload(ctx):
    """ When preloading we need to convert ISO8601 durations into relative
    offsets from the current rungroup start time. As such we depend on
    the rungroup preloader.
    """
    # Get the definition, defaulting to empty def
    schedule_def = ctx.definition.get('schedule', {})
    timings_def = schedule_def.get('timings', {})

    # Only keep the parsed ones
    timings = {}
    for field, offset_multiplier in DEFAULT_OFFSET_MULTIPLIERS.iteritems():
        dt = _resolve_field(
            timings_def.get(field),
            ctx.rungroup, offset_multiplier
        )
        timings[field] = ctx.gettime(dt).isoformat()

    schedule_def['timings'] = timings
    ctx.definition['schedule'] = schedule_def
    return ctx


def _resolve_field(field, rungroup, default_offset_multiplier):
    """ Resolve the field value given the start of the rungroup, the duration
    of the rungroup, and the default offset multipler (number of rungroups)
    """
    # Parse the field as ISO8601 duration
    if field is not None:
        delta = isodate.parse_duration(field)
        # TODO: validate how big deltas can be? Any ideas?
        return rungroup.starts_at + delta

    # Otherwise use the default offset multiplier
    return rungroup.starts_at + (rungroup.period.duration * default_offset_multiplier)
