"""
Django settings for scheduler project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""
import logging
# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
import time
import yaml

JS_UI = False

BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '9m0s26!92tmc#y1a5#9g^p4j*&h8hy)i73#y)tc7+=)4*+*7tw'

# Application Config
CONFIG_FILE = os.environ.get('KAIZEN_CONFIG_FILE', os.path.join(BASE_DIR, 'etc/config.yaml'))
CONFIG = yaml.load(open(CONFIG_FILE, 'r'))


# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATES = [{
    'BACKEND': 'django.template.backends.django.DjangoTemplates',
    'DIRS': [os.path.join(BASE_DIR, 'templates')],
    'OPTIONS': {
        'debug': True,
        'context_processors': [
            'django.template.context_processors.debug',
            'django.template.context_processors.request',
        ],
        'loaders': [
            'django.template.loaders.app_directories.Loader',
        ],
    },
}]

ALLOWED_HOSTS = []

# Application definition

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.humanize',
    'django.contrib.sessions',
    'django.contrib.staticfiles',
    'django_celery_results',
    'django_activeurl',
    'semanticuiform',
    'rest_framework',
    'rest_framework.authtoken',
    'scheduler.accounts',
    'scheduler.repos',
    'scheduler.rungroups',
    'scheduler.jobs',
    'scheduler.definitions',
    'scheduler.dependencies',
    'scheduler.directives',
    'scheduler.filetree',
    'scheduler.events',
    'scheduler.timings',
    'scheduler.routers',
    'scheduler.web'
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'scheduler.accounts.middleware.LoginRequiredMiddleware',
)

ROOT_URLCONF = 'scheduler.urls'

WSGI_APPLICATION = 'scheduler.wsgi.application'


# Celery
# http://docs.celeryproject.org/en/latest/userguide/configuration.html
CELERY_ENABLE_UTC = True
CELERY_RESULT_BACKEND = "django-db"

CELERY_TASK_ACKS_LATE = True
CELERY_TASK_EAGER_PROPAGATES = True
CELERY_TRACK_STARTED = True

CELERY_WORKER_HIJACK_ROOT_LOGGER = False
CELERY_WORKER_REDIRECT_STDOUTS = True


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'kaizen'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = False

# Logging
logging.Formatter.converter = time.gmtime
logging.getLogger("requests").setLevel(logging.WARNING)


def file_handler(name):
    return {
        'level': CONFIG['logging']['level'],
        'class': 'logging.handlers.RotatingFileHandler',
        'maxBytes': 1024*1024*100,  # 100MB
        'backupCount': 5,
        'filename': os.path.join(CONFIG['logging']['directory'], name) + ".log",
        'formatter': 'verbose'
    }

    if not os.path.exists(CONFIG['logging']['directory']):
        os.makedirs(CONFIG['logging']['directory'])

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        },
        'jobs': file_handler('jobs'),
        'dependencies': file_handler('dependencies'),
        'timings': file_handler('timings'),
    },
    'loggers': {
        '': {'handlers': ['console'], 'level': CONFIG['logging']['level'], 'propagate': True},
        'pika': {'handlers': ['console'], 'level': 'WARNING'},
        'scheduler.dependencies': {
            'handlers': ['console', 'dependencies'],
            'level': CONFIG['logging']['level'],
            'propagate': False
        },
        'scheduler.timings': {
            'handlers': ['console', 'timings'],
            'level': CONFIG['logging']['level'],
            'propagate': False
        },
    },
    'formatters': {
        'verbose': {
            'format': '%(asctime)s - %(levelname)s - %(name)s.%(funcName)s:%(lineno)s - %(message)s'
        },
    },
}

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = 'static'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, '../kaizen-ui/dist/'),
)

ACTIVE_URL_KWARGS = {
    'css_class': 'active',
    'parent_tag': 'nav',
    'menu': 'yes'
}
ACTIVE_URL_CACHE = True
ACTIVE_URL_CACHE_TIMEOUT = 60 * 60 * 24  # 1 day
ACTIVE_URL_CACHE_PREFIX = 'django_activeurl'

LOGIN_URL = '/accounts/login/'
LOGIN_REDIRECT_URL = '/'
LOGIN_EXEMPT_URLS = [
    'api/v1/.*',
    'timings/api/v1/.*',
    'dependencies/api/v1/.*'
]

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'scheduler.filetree.backends.NodeBackend',
)

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        # TODO: remove for production
        'rest_framework.authentication.BasicAuthentication',
        # Support session authentication for now (token in future)
        'rest_framework.authentication.SessionAuthentication',
    ),
    'DEFAULT_PERMISSION_CLASSES': (
        # Require every user to be authenticated
        'rest_framework.permissions.IsAuthenticated',
    ),
    'DEFAULT_FILTER_BACKENDS': (
        'django_filters.rest_framework.DjangoFilterBackend',
    )
}

WEBPACK_LOADER = {
    'DEFAULT': {
        'STATS_FILE': os.path.join(BASE_DIR, '../kaizen-ui/webpack-stats.json'),
    }
}
