import django.dispatch

# Send a signal to apps to ask them to perform recovery.
# Recovery is also sent on startup. This is basically the failsafe that catches
# any lost messages and retries them. Given the idempotence requirement on message
# handling we can just send this signal periodically. Perhaps every minute?
do_recovery = django.dispatch.Signal(providing_args=["startup"])
