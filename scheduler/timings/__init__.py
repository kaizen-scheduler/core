import requests
from django.conf import settings


def register(time, message):
    if settings.CONFIG['timings'].get('url') == "LOCAL":
        _register_local(time, message)
    else:
        _register_rest(time, message)


def _register_local(time, message):
    from models import Timing
    Timing.objects.create(time=time, msg=message)


def _register_rest(time, message):
    timing = {
        "time": time,
        "msg": message
    }
    r = requests.post(
        settings.CONFIG['timings']['url'],
        auth=requests.auth.HTTPBasicAuth(
            settings.CONFIG['timings']['username'],
            settings.CONFIG['timings']['password'],
        ),
        json=timing
    )
    r.raise_for_status()
