from django.conf.urls import include, url

urlpatterns = [
    url('^v1/', include('scheduler.timings.api.v1.urls'))
]
