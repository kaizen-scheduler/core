from rest_framework import serializers

from scheduler.timings.models import Timing


class TimingSerializer(serializers.ModelSerializer):
    class Meta:
        model = Timing
