from rest_framework import routers

from viewsets import TimingsViewSet

router = routers.DefaultRouter()
router.register('timings', TimingsViewSet)
urlpatterns = router.urls
