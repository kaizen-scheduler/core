from rest_framework import viewsets

from scheduler.timings.models import Timing
from serializers import TimingSerializer


class TimingsViewSet(viewsets.ModelViewSet):
    queryset = Timing.objects.all()
    serializer_class = TimingSerializer
