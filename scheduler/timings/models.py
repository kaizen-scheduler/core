from django.db import models


class Timing(models.Model):
    """ Store a delayed message """

    # JSON object to send to the callback
    msg = models.TextField()

    # Time at which to send the message
    time = models.DateTimeField()

    # Sent at
    sent_at = models.DateTimeField(null=True, blank=True)
