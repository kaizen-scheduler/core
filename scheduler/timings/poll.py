import json
import logging
import threading
import time
from datetime import datetime, timedelta

from django.conf import settings

from models import Timing
from serializers import TimingCallbackSerializer
import signals

log = logging.getLogger(__name__)


class TimingsPoller(threading.Thread):

    def clean(self):
        """ Purge any timings past expiry """
        # Get purge time
        purge_delta = timedelta(days=settings.CONFIG['timings']['purge_after_days'])
        purge_time = datetime.utcnow() - purge_delta

        # Delete any sent before this time
        Timing.objects.filter(sent_at__lt=purge_time).delete()

    def run(self):
        while True:
            try:
                poll_timings()
                # FIXME: re-enable cleaning when we use Postgres. With SQLite this means we
                # have this thread, and the API threads both writing resulting in locks
                # self.clean()
                time.sleep(5)
            except KeyboardInterrupt:
                break
            except Exception:
                log.exception("Failed to poll timings")
            finally:
                time.sleep(5)


def poll_timings():
    # Send if not sent before, and if expired
    timings = Timing.objects.filter(
        sent_at__isnull=True,
        time__lt=datetime.utcnow()
    )

    n = timings.count()
    log.debug("Publishing timing messages for %d timings", n)

    for t in timings:
        s = TimingCallbackSerializer(t)
        msg = json.loads(s.data['msg'])
        time = s.data['time']
        signals.timing_fired.send_robust(sender=t.id, time=time, msg=msg)
        t.sent_at = datetime.utcnow()
        t.save()
