from rest_framework import serializers

from scheduler.timings.models import Timing


class TimingCallbackSerializer(serializers.ModelSerializer):
    """ Serializer for timing callbacks, limited fields """

    class Meta:
        model = Timing
        fields = ('id', 'msg', 'time')
