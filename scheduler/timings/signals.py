import django.dispatch

timing_fired = django.dispatch.Signal(providing_args=["time", "msg"])
