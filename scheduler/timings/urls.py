from django.conf.urls import include, url

urlpatterns = [
    url('^api/', include('scheduler.timings.api.urls'))
]
