from django.conf import settings
from django.conf.urls import include, url
from django.views.generic import RedirectView

from web.views import ui

# All UIs need the API endpoints
urlpatterns = [
    url('^api/', include('scheduler.api.urls')),
    url('^accounts/', include('scheduler.accounts.urls')),
    url('^timings/', include('scheduler.timings.urls')),
]

if settings.JS_UI:
    urlpatterns += [url('^$', ui)]
else:
    urlpatterns += [
        url('^$', RedirectView.as_view(url='/instances/')),
        url('^browse/', include('scheduler.filetree.urls')),
        url('^directives/', include('scheduler.directives.urls')),
        url('^repos/', include('scheduler.repos.urls')),
        url('^', include('scheduler.jobs.urls'))
    ]
