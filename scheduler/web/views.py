from django.shortcuts import render


def ui(request):
    return render(request, 'scheduler.web/index.html', {})
