from django.contrib.auth.models import Group, User
import factory
from factory.django import DjangoModelFactory


class UserFactory(DjangoModelFactory):

    class Meta:
        model = User
        django_get_or_create = ('username',)

    username = 'test'
    email = 'test@test.com'
    password = factory.PostGenerationMethodCall('set_password', 'password')


class GroupFactory(DjangoModelFactory):

    class Meta:
        model = Group
        django_get_or_create = ('name',)

    name = 'testgroup'
