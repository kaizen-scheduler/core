from pytest_factoryboy import register
from factories import GroupFactory, UserFactory


register(GroupFactory)
register(UserFactory)
register(UserFactory, 'other_user', username='other')
