import pytest
from scheduler.jobs import states

PROC_ROOT = "tests/config/discovery"


@pytest.mark.django_db
def test_discovery(ji_by_name, poll_timings):
    # Load the producer first
    producer = ji_by_name('producer', root=PROC_ROOT)
    poll_timings()
    assert producer.states.filter(name=states.STATE_RUNNING).exists()

    consumer = ji_by_name('consumer', root=PROC_ROOT)
    assert consumer.resolved['command'] == 'echo world'
