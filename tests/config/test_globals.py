from scheduler import config


def test_globals(settings):
    """ Test that globals are substituted into the definition.
    We do this by overriding the Django settings and then assert the globals
    are substituted into the definition
    """
    settings.CONFIG['globals'] = {
        'hello': 'world',
        'int': 123,
        'bool': True,
        'float': 1.23,
    }

    definition = {
        "hello": "{{ globals.hello }}",
        "int": "{{ globals.int | int }}",
        "bool": "{{ globals.bool | bool }}",
        "float": "{{ globals.float | float }}",
    }

    expected = {
        "hello": "world",
        "int": 123,
        "bool": True,
        "float": 1.23,
    }

    assert config.apply_templating(definition) == expected
