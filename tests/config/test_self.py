import pytest

PROC_ROOT = "tests/config/self"


@pytest.mark.django_db
def test_self(ji_by_name):
    """ Test that we can read values from our own definition """
    ji = ji_by_name('self', root=PROC_ROOT)

    expected = "echo %s" % ji.rungroup
    assert ji.resolved['command'] == expected
