pytest_plugins = [
    "tests.fixtures",
    "tests.accounts.fixtures",
    "tests.definitions.fixtures",
    "tests.filetree.fixtures",
    "tests.jobs.fixtures",
    "tests.repos.fixtures",
    "tests.rungroups.fixtures",
    "tests.timings.fixtures",
]
