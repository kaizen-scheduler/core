from factory import SubFactory
from factory.django import DjangoModelFactory
from scheduler.definitions import models
from tests.repos.factories import RepoFactory


class JobDefinitionFileFactory(DjangoModelFactory):

    class Meta:
        model = models.JobDefinitionFile


class JobDefinitionFactory(DjangoModelFactory):

    class Meta:
        model = models.JobDefinition

    name = "test/job"
    runperiod = "D"
    repo = SubFactory(RepoFactory)
