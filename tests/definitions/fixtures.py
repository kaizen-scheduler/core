from pytest_factoryboy import register
from factories import JobDefinitionFactory, JobDefinitionFileFactory


register(JobDefinitionFactory)
register(JobDefinitionFileFactory)
