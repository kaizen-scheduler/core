import pytest

from scheduler.definitions import parser
from scheduler.definitions.models import JobDefinition

NAME = "/test/definitions"
CONTENT = """{
   "schedule": {"period": "hourly"}
}"""
RUNPERIOD = "hourly"


def test_parse_bad_json():
    """ Test that malformed JSON results in a parsed definition with errors """
    pd = parser._parse_definition(NAME, "{hello]")
    assert len(pd['errors']) == 1


def test_parse_not_dict():
    """ Test that anything but a JSON dict results in an error """
    pd = parser._parse_definition(NAME, "[1,2,3]")
    assert len(pd['errors']) == 1
    pd = parser._parse_definition(NAME, "\"hello\"")
    assert len(pd['errors']) == 1
    pd = parser._parse_definition(NAME, "123")
    assert len(pd['errors']) == 1


def test_parse():
    """ Test a valid definition.
    TODO: test particular parse plugins """
    pd = parser._parse_definition(NAME, CONTENT)
    assert len(pd['errors']) == 0
    assert len(pd['warnings']) == 0
    assert pd['name'] == NAME


@pytest.mark.django_db
def test_full_parse(repo):
    """ Test that the end-to-end parsing works """
    jd = parser.parse(NAME, CONTENT, repo)
    assert jd.active
    assert jd.runperiod == RUNPERIOD

    # Now test that parsing the same name results in one active
    # and one inactive jd
    jd2 = parser.parse(NAME, CONTENT, repo)
    assert jd2.active

    jds = JobDefinition.objects.filter(name=NAME)
    assert jds.count() == 2
    assert jds.filter(active=True).count() == 1
    assert jds.filter(active=False).count() == 1
