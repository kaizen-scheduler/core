import pytest

from scheduler.dependencies import lib
from scheduler.dependencies.models import Reference


@pytest.mark.django_db
def test_create_empty_dependencies():
    lib.create_dependencies("A", [])

    with pytest.raises(Reference.DoesNotExist):
        Reference.dependencies("B")

    assert Reference.dependencies("A") == []


@pytest.mark.django_db
def test_create_dependencies():
    lib.create_dependencies("A", [{"B": "STATE", "C": "STATE"}])

    assert set(Reference.dependencies("A")) == set(["B", "C"])
    assert Reference.dependents("B") == ["A"]
    assert Reference.dependents("C") == ["A"]


@pytest.mark.django_db
def test_upate_reference():
    lib.create_dependencies("A", [{"B": "STATE", "C": "STATE"}])
    lib.update_reference("A", "Z")

    assert set(Reference.dependencies("Z")) == set(["B", "C"])
    assert Reference.dependents("B") == ["Z"]
    assert Reference.dependents("C") == ["Z"]

    with pytest.raises(Reference.DoesNotExist):
        Reference.dependencies("A")


@pytest.mark.django_db
def test_empty_satisfy():
    lib.create_dependencies("A", [])
    assert lib.is_satisfied("A")
    assert lib.is_satisfied("B")


@pytest.mark.django_db
def test_AND_satisfy():
    lib.create_dependencies("A", [{"B": "STATE", "C": "STATE"}])

    assert not lib.is_satisfied("A")

    lib.satisfy("B", "STATE")
    assert not lib.is_satisfied("A")

    lib.satisfy("C", "STATE")
    assert lib.is_satisfied("A")


@pytest.mark.django_db
def test_OR_satisfy():
    lib.create_dependencies("A", [{"B": "STATE"}, {"C": "STATE"}])

    assert not lib.is_satisfied("A")

    lib.satisfy("B", "STATE")
    assert lib.is_satisfied("A")


@pytest.mark.django_db
def test_AND_OR_satisfy():
    lib.create_dependencies("A", [{"B": "STATE", "C": "STATE"}, {"D": "STATE"}])

    assert not lib.is_satisfied("A")

    lib.satisfy("B", "STATE")
    assert not lib.is_satisfied("A")

    lib.satisfy("C", "STATE")
    assert lib.is_satisfied("A")
