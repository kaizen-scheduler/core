import pytest

from scheduler.dependencies.models import Reference


@pytest.mark.django_db
def test_dep_relations():
    a = Reference.objects.create(ref="A")
    b = Reference.objects.create(ref="B")
    b.add_parent(a, and_idx=0)

    assert Reference.dependencies("A") == []
    assert Reference.dependencies("B") == ["A"]
    assert Reference.dependents("A") == ["B"]
    assert Reference.dependents("B") == []


@pytest.mark.django_db
def test_transitive_dep_relations():
    a = Reference.objects.create(ref="A")
    b = Reference.objects.create(ref="B")
    c = Reference.objects.create(ref="C")
    b.add_parent(a, and_idx=0)
    c.add_parent(b, and_idx=0)

    assert Reference.dependencies("C") == ["B"]
    assert Reference.dependencies("B") == ["A"]
    assert Reference.dependents("A") == ["B"]
    assert Reference.dependents("B") == ["C"]
    assert set(Reference.all_dependencies("C")) == set(["A", "B"])
    assert set(Reference.all_dependents("A")) == set(["B", "C"])
