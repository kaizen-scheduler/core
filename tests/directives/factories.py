import factory
from factory.django import DjangoModelFactory
from scheduler.directives.models import Directive
from tests.accounts.factories import UserFactory


class DirectiveFactory(DjangoModelFactory):

    class Meta:
        model = Directive

    created_by = factory.SubFactory(UserFactory)
