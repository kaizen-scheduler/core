import json
from scheduler.directives.models import Directive
from factories import DirectiveFactory


def issue_kill_job(jids):
    d = DirectiveFactory(type=Directive.KILL_JI_DIRECTIVE)
    d.job_instances.add(*jids)
    return d.run()


def issue_run_job(jids):
    d = DirectiveFactory(type=Directive.RUN_JI_DIRECTIVE)
    d.job_instances.add(*jids)
    return d.run()


def issue_load_rungroup(rungroup):
    d = DirectiveFactory(
        type=Directive.LOAD_RUNGROUP_DIRECTIVE,
        parameters=json.dumps({'rungroup': str(rungroup)})
    )
    return d.run()


def issue_load_job(job_id, rungroup, force=False):
    d = DirectiveFactory(
        type=Directive.LOAD_JOB_DIRECTIVE,
        parameters=json.dumps({
            'job_id': str(job_id),
            'rungroup': str(rungroup),
            'force': force
        })
    )
    return d.run()
