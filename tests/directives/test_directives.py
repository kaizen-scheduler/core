import pytest
from django.core.urlresolvers import reverse

from scheduler.directives.models import Directive


@pytest.mark.django_db
def test_login_required(client):
    """ Assert for a non-logged-in client we get redirected to login """
    resp = client.get(reverse('directives:list'))
    assert resp.status_code == 302
    assert resp.url == reverse('accounts:login')


@pytest.mark.django_db
def test_empty_list_directives(user_client):
    """ Test we get a 200 and an empty directive list """
    resp = user_client.get(reverse('directives:list'))
    assert resp.status_code == 200
    ctx = resp.context[0]
    assert ctx['directive_list'].count() == 0


@pytest.mark.django_db
def test_list_directives(user_client, user):
    """ Having created a directive, assert we see it in the view """
    Directive.objects.create(
        type=Directive.LOAD_RUNGROUP_DIRECTIVE,
        parameters='{"rungroup": "D20160101"}',
        created_by=user
    )

    resp = user_client.get(reverse('directives:list'))
    assert resp.status_code == 200
    ctx = resp.context[0]
    assert ctx['directive_list'].count() == 1
