import celery
from celery.canvas import _chain, group, Signature
from scheduler.directives import graph as g


@celery.shared_task
def task_1():
    pass


@celery.shared_task
def task_2():
    pass


T1 = task_1.si()
T2 = task_2.si()


def test_single_task_graph():
    start, ends, graph = g._compute_single_task_graph({}, T1)
    assert start == T1.id
    assert ends == [T1.id]
    assert graph == {T1.id: []}


def test_chain_task_graph():
    chain = T1 | T2
    start, ends, graph = g._compute_chain_graph({}, chain)
    assert start == T1.id
    assert ends == [T2.id]
    assert graph == {T1.id: [T2.id], T2.id: []}


def test_group_graph():
    grp = group(T1, T2)
    grp.freeze()
    start, ends, graph = g._compute_group_graph({}, grp)
    assert start == grp.id
    assert set(ends) == set([T1.id, T2.id])
    assert graph == {grp.id: [T1.id, T2.id], T1.id: [], T2.id: []}
