import pytest
from django.core.urlresolvers import reverse

pytestmark = pytest.mark.django_db


def get_resp(client, view, params):
    return client.post(reverse(view), params)


def get_form(client, view, params):
    ctx = get_resp(client, view, params).context[0]
    return ctx['form']


def test_load_rungroup_view(user_client, rungroup):
    # Empty params is invalid
    form = get_form(user_client, 'directives:issue:load_rungroup', {})
    assert not form.is_valid()

    # Submit a rungroup and check we're redirected to the directive page
    params = {'type': 'load_rungroup', 'rungroup': str(rungroup)}
    resp = get_resp(user_client, 'directives:issue:load_rungroup', params)
    try:
        assert resp.status_code == 302
    except:
        print resp.content
        raise
