from celery.exceptions import MaxRetriesExceededError
import pytest
from scheduler.directives.exceptions import InvalidJobStateError
from scheduler.jobs import states
from issue import issue_kill_job

PROC_ROOT = "tests/directives/kill_job"


@pytest.mark.django_db
def test_kill_loaded(ji_by_name):
    """ Killing a ji in LOADED state should do nothing and the directive should raise """
    ji = ji_by_name("state_loaded", root=PROC_ROOT)
    assert ji.states.latest().name == states.STATE_LOADED
    with pytest.raises(InvalidJobStateError):
        issue_kill_job([ji.id])
    assert ji.states.latest().name == states.STATE_LOADED


@pytest.mark.django_db
def test_kill_launched(ji_by_name, poll_timings):
    """ Killing a ji in LAUNCHED state should result in FAILED. """
    ji = ji_by_name("state_launched", root=PROC_ROOT)
    assert ji.states.latest().name == states.STATE_LOADED
    poll_timings()
    assert ji.states.latest().name == states.STATE_LAUNCHED

    d = issue_kill_job([ji.id])
    assert ji.states.latest().name == states.STATE_FAILED

    # Assert that the directive has a single task
    assert d.tasks.count() == 1


@pytest.mark.django_db
def test_kill_multiple_jis(ji_by_name, poll_timings):
    """ Killing multiple jis should block waiting for all the be killed """
    ji1 = ji_by_name("state_launched", root=PROC_ROOT)
    ji2 = ji_by_name("state_running", root=PROC_ROOT)
    poll_timings()

    d = issue_kill_job([ji1.id, ji2.id])
    # Assert that the directive has two tasks
    assert d.tasks.count() == 2

    assert ji1.states.latest().name == states.STATE_FAILED
    assert ji2.states.latest().name == states.STATE_FAILED


@pytest.mark.django_db
def test_kill_no_router_response(ji_by_name, poll_timings):
    """ Killing a ji in LAUNCHED state and router doesn't respond should result
    in failed directive and the ji in KILLED state """
    ji = ji_by_name("hung_launch", root=PROC_ROOT)
    assert ji.states.latest().name == states.STATE_LOADED
    poll_timings()
    assert ji.states.latest().name == states.STATE_LAUNCHED

    with pytest.raises(MaxRetriesExceededError):
        issue_kill_job([ji.id])
    assert ji.states.latest().name == states.STATE_KILLED
