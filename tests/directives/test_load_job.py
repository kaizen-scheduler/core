import pytest

from scheduler.jobs import instance, states
from issue import issue_load_job

PROC_ROOT = "tests/directives/load_job"


@pytest.mark.django_db
def test_load_job(job_by_name, rungroup, poll_timings):
    """ Submit a load_job directive """
    job = job_by_name("state_running", root=PROC_ROOT)
    d = issue_load_job(job.id, rungroup)

    # Assert we have a single directive
    assert d.tasks.count() == 1

    # Assert that we now have a JobInstance for the given job
    assert job.instances.count() == 1
    assert job.instances.latest().state == states.STATE_LOADED

    # Trigger the timings message and check we're now launched
    poll_timings()
    assert job.instances.latest().state == states.STATE_LAUNCHED


@pytest.mark.django_db
def test_failed_load_daily_job(job_by_name, rungroup):
    """ Test a job that fails to load because of malformed definition.

    The directive should fail and there should be no JobInstance afterwards.
    """
    job = job_by_name("load_fails", root=PROC_ROOT)
    with pytest.raises(Exception):
        issue_load_job(job.id, rungroup)

    # Assert that we don't have a JobInstance for the given job
    assert job.instances.count() == 0


@pytest.mark.django_db
def test_no_force_reload_job(job_by_name, rungroup):
    """ When we load_job twice without forcing we shouldn't create a new JobInstance """
    job = job_by_name("state_running", root=PROC_ROOT)

    # Assert that we now have a JobInstance for the given job after loading
    issue_load_job(job.id, rungroup)
    assert job.instances.count() == 1

    # Assert that we still have only one JobInstance for the given job after reload
    issue_load_job(job.id, rungroup)
    assert job.instances.count() == 1


@pytest.mark.django_db
def test_force_reload_job(ji_by_name, poll_timings):
    """ When we load_job twice without forcing we shouldn't create a new JobInstance """
    ji = ji_by_name("state_running", root=PROC_ROOT)

    # Assert that we now have a JobInstance for the given job after loading
    assert ji.job.instances.count() == 1
    poll_timings()
    assert instance.has_state(ji.id, states.STATE_RUNNING)

    # Assert that we have two JobInstances for the given job after reload
    d = issue_load_job(ji.job.id, ji.rungroup, force=True)
    assert ji.job.instances.count() == 2

    # And that we used two tasks (kill + reload) to do it
    assert d.tasks.count() == 2

    # Assert that the first ji is now killed and failed
    assert instance.has_state(ji.id, states.STATE_KILLED)
    assert ji.states.latest().name == states.STATE_FAILED

    # Check the new one is running
    assert ji.job.instances.latest().state == states.STATE_LOADED


@pytest.mark.django_db
def test_load_job_graph(ji_by_name, poll_timings):
    """ When we load a job we expect the graph to... """
    ji = ji_by_name("state_running", root=PROC_ROOT)
    poll_timings()
    assert instance.has_state(ji.id, states.STATE_RUNNING)

    import json
    from scheduler.directives.models import Directive
    from scheduler.directives import directives
    from tests.directives.factories import DirectiveFactory
    d = DirectiveFactory(
        type=Directive.LOAD_JOB_DIRECTIVE,
        parameters=json.dumps({
            'job_id': str(ji.job.id),
            'rungroup': str(ji.rungroup),
            'force': True
        })
    )
    lj = directives.load_job(d)

    assert not d.graph
    assert not lj.build_graph()
