import pytest
from issue import issue_load_rungroup
from scheduler.jobs.models import JobInstance
from scheduler.rungroups.models import RunGroup

RUNGROUP_ROOT = "tests/directives/load_rungroup/"


@pytest.mark.django_db
@pytest.mark.parametrize("repo__root", [RUNGROUP_ROOT])
def test_load_daily_rungroup(repo, rungroup):
    """ Submit a load_rungroup directive """
    assert JobInstance.objects.count() == 0
    issue_load_rungroup(rungroup)

    # We only have one daily rungroup job in the repo
    assert JobInstance.objects.count() == 1
    # TODO: use RunGroup model
    assert RunGroup.from_string(JobInstance.objects.all()[0].rungroup).period.name == "daily"


@pytest.mark.django_db
@pytest.mark.parametrize("repo__root", [RUNGROUP_ROOT])
def test_load_hourly_rungroup(repo, hourly_rungroup):
    """ Submit a load_rungroup directive """
    assert JobInstance.objects.count() == 0
    issue_load_rungroup(hourly_rungroup)

    # We only have one hourly rungroup job in the repo
    assert JobInstance.objects.count() == 1
    # TODO: use RunGroup model
    assert RunGroup.from_string(JobInstance.objects.all()[0].rungroup).period.name == "hourly"
