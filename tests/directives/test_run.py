import pytest
from scheduler.directives.exceptions import InvalidJobStateError
from scheduler.jobs import instance, states
from issue import issue_run_job

PROC_ROOT = "tests/directives/run_job"


@pytest.mark.django_db
def test_run_running(ji_by_name, poll_timings):
    """ Running a job that isn't in LOADED should throw an InvalidJobStateError """
    ji = ji_by_name("state_running", root=PROC_ROOT)
    poll_timings()
    assert instance.has_state(ji.id, states.STATE_RUNNING)

    with pytest.raises(InvalidJobStateError):
        issue_run_job([ji.id])

    assert instance.has_state(ji.id, states.STATE_RUNNING)


@pytest.mark.django_db
def test_run_loaded(ji_by_name, poll_timings):
    """ Running a job that is LOADED (due to some unmet dependency) should result in
    that job being launched and running """
    ji = ji_by_name("state_loaded", root=PROC_ROOT)
    assert ji.states.latest().name == states.STATE_LOADED
    issue_run_job([ji.id])
    assert ji.states.latest().name == states.STATE_LAUNCHED
