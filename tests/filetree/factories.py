import factory
from factory.django import DjangoModelFactory
from scheduler.filetree.models import Directory, Node
from tests.accounts.factories import GroupFactory, UserFactory


class NodeFactory(DjangoModelFactory):
    class Meta:
        model = Node
        django_get_or_create = ('path',)

    path = "/test"
    parent_id = "/"
    user = factory.SubFactory(UserFactory)
    group = factory.SubFactory(GroupFactory)


class DirectoryFactory(DjangoModelFactory):
    class Meta:
        model = Directory
        django_get_or_create = ('path',)

    path = "/test"
    parent_id = "/"
    user = factory.SubFactory(UserFactory)
    group = factory.SubFactory(GroupFactory)
