from pytest_factoryboy import register
from factories import DirectoryFactory, NodeFactory


register(NodeFactory)
register(DirectoryFactory)
register(DirectoryFactory, 'root', path="/", parent_id="/")
