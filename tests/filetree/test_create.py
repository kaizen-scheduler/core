import pytest
from scheduler.filetree.models import Node
from tests.accounts.factories import UserFactory
from factories import DirectoryFactory, NodeFactory


@pytest.mark.django_db
def test_create_no_path():
    # This has a risk of infinite recursion with os.path.dirname...
    with pytest.raises(Exception):
        n = NodeFactory.build(path="")
        n.save(mkdirs=True)


@pytest.mark.django_db
def test_create_no_parent():
    assert not Node.objects.filter(path="/").exists()
    with pytest.raises(Node.DoesNotExist):
        NodeFactory.create(path="/hello")
    with pytest.raises(Node.DoesNotExist):
        NodeFactory.create(path="/hello/world")


@pytest.mark.django_db
def test_create_with_parent(root):
    n = NodeFactory.create(path="/hello")
    assert not n.is_directory()
    assert n.path == "/hello"


@pytest.mark.django_db
def test_create_in_file(root):
    n = NodeFactory.create(path="/hello")
    assert not n.is_directory()
    assert n.path == "/hello"

    with pytest.raises(Exception):
        # Should fail since /hello is a file
        NodeFactory.create(path="/hello/world/")


@pytest.mark.django_db
def test_create_dirs(root, user, group):
    n = NodeFactory.build(path="/hello/world", user=user, group=group)
    n.save(mkdirs=True)


@pytest.mark.django_db
def test_parent_not_writable():
    DirectoryFactory.create(path="/", parent_id="/", mode=0555)
    with pytest.raises(Node.PermissionDenied):
        NodeFactory.create(path="/hello")


@pytest.mark.django_db
def test_permission_denied(user):
    root_user = UserFactory(username='root')
    root = DirectoryFactory.create(path="/", parent_id="/", user=root_user, mode=0775)

    # Should fail when trying to create a subdirectory as a different user
    with pytest.raises(Node.PermissionDenied):
        NodeFactory.create(path="/hello", user=user)

    # Also check that if we can't write to the root then no nodes at all are created
    # i.e. create nodes from top-down to ensure we have permissions all the way down
    node_count = Node.objects.count()
    with pytest.raises(Node.PermissionDenied):
        n = NodeFactory.build(path="/hello/world/foo/bar/", user=user)
        n.save(mkdirs=True)
    assert node_count == Node.objects.count()

    # Now add the user to the parent's group and check it's now writable
    user.groups.add(root.group)
    NodeFactory.create(path="/hello", user=user)
