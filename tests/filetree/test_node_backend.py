import pytest
from factories import NodeFactory
from scheduler.filetree.models import NodePermission

pytestmark = pytest.mark.django_db


@pytest.mark.parametrize('mode,perm,expected', [
    (0400, NodePermission.R, True),
    (0400, NodePermission.W, False),
    (0400, NodePermission.X, False),
    (0040, NodePermission.R, False),
    (0100, NodePermission.R, False),
    (0100, NodePermission.X, True),
    (0007, NodePermission.R, True),
    (0007, NodePermission.W, True),
    (0007, NodePermission.X, True),
    # Let's not go crazy, these are mostly tested elswhere
])
def test_owner_has_perm(root, user, mode, perm, expected):
    n = NodeFactory(mode=mode)
    assert user.has_perm(perm, obj=n) == expected
