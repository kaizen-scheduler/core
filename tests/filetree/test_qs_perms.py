import pytest
from scheduler.filetree.models import Node
from factories import NodeFactory
from tests.accounts.factories import GroupFactory, UserFactory


@pytest.mark.django_db
def test_world_readable(root, user, group):
    NodeFactory.create(path="/r", mode=0004)
    NodeFactory.create(path="/nr", mode=0400)

    assert Node.objects.readable().filter(path="/r").exists()
    assert not Node.objects.readable().filter(path="/nr").exists()


@pytest.mark.django_db
def test_user_readable(root, user, group):
    NodeFactory.create(path="/r", mode=0400)

    other = GroupFactory.create(name="other")
    NodeFactory.create(path="/nr", mode=0040, group=other)

    assert Node.objects.readable(user=user).filter(path="/r").exists()
    assert not Node.objects.readable(user=user).filter(path="/nr").exists()


@pytest.mark.django_db
def test_group_readable(root, user, group):
    NodeFactory.create(path="/r", mode=0040)

    other = GroupFactory.create(name="other")
    NodeFactory.create(path="/nr", mode=0040, group=other)

    # 'user' is only a member of 'group', not of 'other'
    user.groups.add(group)
    assert group in user.groups.all()
    assert other not in user.groups.all()

    assert Node.objects.readable(user=user).filter(path="/r").exists()
    assert not Node.objects.readable(user=user).filter(path="/nr").exists()
