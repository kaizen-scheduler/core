import pytest
from django.conf import settings
from django.test.client import Client


@pytest.fixture(scope='session')
def services(live_server):
    settings.CONFIG['timings']['url'] = live_server + '/timings/api/v1/timings/'


@pytest.fixture()
def user_client(user):
    """A Django test client logged in as a non-admin user."""
    client = Client()
    assert client.login(username=user.username, password='password')
    return client
