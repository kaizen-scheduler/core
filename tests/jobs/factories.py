import factory
from factory.django import DjangoModelFactory
from scheduler.jobs import models
from tests.definitions.factories import JobDefinitionFactory
from tests.repos.factories import RepoFactory


class JobFactory(DjangoModelFactory):

    class Meta:
        model = models.Job

    repo = factory.SubFactory(RepoFactory)


class JobInstanceFactory(DjangoModelFactory):

    class Meta:
        model = models.JobInstance

    definition = factory.SubFactory(JobDefinitionFactory)
    job = factory.SubFactory(JobFactory)
    repo = factory.SubFactory(RepoFactory)
