import os
from django.conf import settings
import pytest
from pytest_factoryboy import register
from factories import JobFactory, JobInstanceFactory
from scheduler.jobs import instance
from scheduler.jobs.models import Job, JobInstance
from tests.repos.factories import RepoFactory


register(JobFactory)
register(JobInstanceFactory)


@pytest.fixture()
def job_by_name():
    def get_job(name, root=settings.TEST_PROCS_ROOT):
        repo = RepoFactory.create(root=root)
        return Job.objects.get(job_repo=repo, path=os.path.join(repo.path, name))
    return get_job


@pytest.fixture()
def ji_by_name(job_by_name, rungroup):
    def get_ji(name, root=settings.TEST_PROCS_ROOT):
        job = job_by_name(name, root=root)
        # Force load gives us a new jid each time
        jid = instance.instantiate_job(job.id, rungroup, force=True)
        return JobInstance.objects.get(id=jid)
    return get_ji
