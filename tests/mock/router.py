import logging
from scheduler.jobs.routing import callbacks
from scheduler.routers.client import BaseRouter

log = logging.getLogger(__name__)


class MockRouter(BaseRouter):
    """ A well-behaved router than notifies of running / failed tasks. """

    def launch(self, ji):
        log.info("Launching %s", ji)
        callbacks.running(ji)

    def kill(self, ji):
        log.info("Killing %s", ji)
        callbacks.failed(ji)


class MissingRouter(BaseRouter):
    """ The router raises an exception on any call, as it would if the
    router didn't exist at the given URL """

    def launch(self, ji):
        raise Exception("Router missing")

    def kill(self, ji):
        raise Exception("Router missing")


class SilentRouter(BaseRouter):
    """ The router just doesn't respond... """

    def launch(self, ji):
        pass

    def kill(self, ji):
        pass


class DiscoveryRouter(BaseRouter):
    """ A well-behaved router than also includes discovery config """

    def launch(self, ji):
        log.info("Launching %s", ji)
        callbacks.running(ji, data={'discovery': {'hello': 'world'}})

    def kill(self, ji):
        log.info("Killing %s", ji)
        callbacks.failed(ji, data={'discovery': {'hello': 'noone'}})
