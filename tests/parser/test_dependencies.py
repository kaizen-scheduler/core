import pytest

from scheduler.dependencies.parser import _normalise_dependencies
from scheduler.jobs.states import STATE_FAILED as F
from scheduler.jobs.states import STATE_SUCCEEDED as S


@pytest.mark.parametrize("deps_in,deps_out", [
    # Single string, single dep
    ("", []),
    ([], []),

    # Single string, single dep
    ("A", [{"A": S}]),

    # List of strings should be conjunction
    (["A", "B"], [{"A": S, "B": S}]),

    # Single dict, single dep
    ({"A": F}, [{"A": F}]),
    ({"A": F, "B": S}, [{"A": F, "B": S}]),

    # List of list of strings should be conjunction
    ([{"A": F, "B": S}], [{"A": F, "B": S}]),

    # Multiple list of list of strings should be disjunction
    ([{"A": S, "B": F}, {"B": S}], [{"A": S, "B": F}, {"B": S}]),
])
def test_normalise(deps_in, deps_out):
    assert _normalise_dependencies(deps_in) == deps_out
