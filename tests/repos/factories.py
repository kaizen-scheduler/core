from django.conf import settings
import factory
from factory.django import DjangoModelFactory
from scheduler.repos.models import Repo
from tests.accounts.factories import GroupFactory, UserFactory


class RepoFactory(DjangoModelFactory):
    class Meta:
        model = Repo
        django_get_or_create = ('url', 'commitish', 'root')

    @classmethod
    def _create(cls, model_class, *args, **kwargs):
        """ Override create so we can save with mkdirs=True.

        First we grab the kwargs for get_or_create, falling back to save(mkdirs=True)
        if we cannot find an existing instance
        """
        goc = {k: v for k, v in kwargs.items() if k in cls._meta.django_get_or_create}
        try:
            repo = Repo.objects.get(**goc)
        except Repo.DoesNotExist:
            repo = model_class(*args, **kwargs)
            repo.save(mkdirs=True)

        # Make sure the user is in the group
        # This is the wrong place for this, but will do for now
        repo.user.groups.add(repo.group)

        return repo

    path = "/test/repo/"
    user = factory.SubFactory(UserFactory)
    group = factory.SubFactory(GroupFactory)
    description = "Test Repository"
    # TODO: point at the example-procs repo? Support file:// repo not git://
    url = settings.TEST_PROCS_REPO
    root = settings.TEST_PROCS_ROOT
    commitish = 'master'
