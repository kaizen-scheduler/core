from django.core.urlresolvers import reverse
import pytest
from tests.repos.factories import RepoFactory

pytestmark = pytest.mark.django_db


def resp(user_client, repo):
    return user_client.get(reverse('repos:detail', args=(repo.id,)))


def test_repo_perms(user_client, repo, user):
    assert resp(user_client, repo).status_code == 200


def test_repo_other_user_perms(user_client, other_user):
    # Create a repo owned by someone else
    repo = RepoFactory.create(mode=0700, user=other_user)
    assert resp(user_client, repo).status_code == 403


def test_repo_other_user_group_readable_perms(user_client, user, other_user):
    # Create a repo owned by someone else
    repo = RepoFactory.create(mode=0770, user=other_user)

    # The user cannot read it
    assert resp(user_client, repo).status_code == 403

    # Until they're in that group
    repo.group.user_set.add(user)
    assert resp(user_client, repo).status_code == 200


def test_repo_other_user_world_readable_perms(user_client, other_user):
    # Create a repo owned by someone else
    repo = RepoFactory.create(mode=0707, user=other_user)
    assert resp(user_client, repo).status_code == 200
