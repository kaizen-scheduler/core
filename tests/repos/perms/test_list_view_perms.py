from django.core.urlresolvers import reverse
import pytest
from tests.repos.factories import RepoFactory

pytestmark = pytest.mark.django_db


def repo_list(user_client):
    resp = user_client.get(reverse('repos:list'))
    ctx = resp.context[0]
    return ctx['repo_list']


def test_empty_repo_list_perms(user_client):
    # No repos, just sanity check the repo list is empty
    assert repo_list(user_client).count() == 0


def test_repo_list_perms(user_client, user):
    # Create a repo owned by ourselves
    RepoFactory.create(mode=0700, user=user)
    assert repo_list(user_client).count() == 1


def test_repo_list_other_user_perms(user_client, other_user):
    # Create a repo owned by someone else
    RepoFactory.create(mode=0700, user=other_user)
    assert repo_list(user_client).count() == 0
