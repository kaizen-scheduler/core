import pytest
from django.conf import settings
from scheduler.repos.models import Repo

REPO_PATH = "/my/test/repo/"
PROCS_ROOT = "tests/repos"


@pytest.mark.django_db
def test_repo_create(user, group):
    """ Test creating a repository and check the associated nodes were created """
    r = Repo(
        path=REPO_PATH,
        description="Test Repo Description",
        user=user, group=group,
        url=settings.TEST_PROCS_REPO, root=PROCS_ROOT,
        commitish="HEAD"
    )
    r.save(mkdirs=True)

    # Check the repo is a directory
    assert r.is_directory()

    # Check the job path is within the repo path and that user/group are the same
    job = r.jobs.first()
    assert not job.is_directory()
    assert job.path.startswith(REPO_PATH)
    assert job.user == user
    assert job.group == group


def test_repo_create_duplicate_mount():
    """ Test creating the repo at a duplicate or parent mount point """
    pass


def test_repo_delete():
    """ Test that deleting a repo removes associated entities such as nodes """
    pass
