import pytest
from scheduler.rungroups.models import RunGroup


@pytest.fixture()
def rungroup():
    return RunGroup.from_string("D20160101")


@pytest.fixture()
def hourly_rungroup():
    return RunGroup.from_string("H20160101:01")
