import os
import pytest
import tempfile

from scheduler.rungroups.models import RunGroup
from scheduler.rungroups.filters import CalendarFilter, NotInSchedule

NEW_YEARS_DAY = """
BEGIN:VCALENDAR
VERSION:2.0
PRODID:ics.py - http://git.io/lLljaA
BEGIN:VEVENT
DTSTAMP:20170110T133209Z
DTSTART:20160101T000000Z
DTEND:20160102T000000Z
SUMMARY:New Years Day
UID:b5945e9b-3846-4b73-8912-06a40829f575@b594.org
END:VEVENT
END:VCALENDAR
"""


@pytest.fixture
def cal():
    _fd, tmp = tempfile.mkstemp()
    with open(tmp, 'w+') as f:
        f.write(NEW_YEARS_DAY)
    yield tmp
    os.unlink(tmp)


def test_calendar_filter(cal):
    # New years day 2016
    nyd = RunGroup.from_string("D20160101")
    nyd2 = RunGroup.from_string("D20160102")

    f = CalendarFilter("file://" + cal)
    f.assert_matches(nyd)
    with pytest.raises(NotInSchedule):
        f.assert_matches(nyd2)


def test_negated_calendar_filter(cal):
    # New years day 2016
    nyd = RunGroup.from_string("D20160101")
    nyd2 = RunGroup.from_string("D20160102")

    f = CalendarFilter("!file://" + cal)
    with pytest.raises(NotInSchedule):
        f.assert_matches(nyd)
    f.assert_matches(nyd2)
