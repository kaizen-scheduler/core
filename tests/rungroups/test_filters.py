import pytest
from scheduler.rungroups.models import RunGroup
from scheduler.rungroups import filters


@pytest.mark.parametrize("value, expected", [
    ("0", ["0"]),
    ("23", ["23"]),
    ("0,1", ["0", "1"]),
    ("0-3", ["0", "1", "2", "3"]),
    ("5,0-3", ["0", "1", "2", "3", "5"]),
    ("22-3", ["22", "23", "0", "1", "2", "3"]),
])
def test_parse_options(value, expected):
    OPTS = [str(i) for i in range(0, 24)]
    assert set(filters._parse_options(value, OPTS)) == set(expected)


@pytest.mark.parametrize("rungroup_str, value", [
    # D20160101 was a Friday
    ('D20160101', 'monday'),
    ('D20160101', 'tuesday-thursday'),
    ('D20160101', 'tuesday-thursday,saturday, sunday'),
])
def test_not_in_day_filter(rungroup_str, value):
    with pytest.raises(filters.NotInSchedule):
        filters.DayFilter(value).assert_matches(RunGroup.from_string(rungroup_str))


@pytest.mark.parametrize("rungroup_str, value", [
    # D20160101 was a Friday
    ('D20160101', None),
    ('D20160101', 'friday'),
    ('D20160101', 'monday-friday'),
    ('D20160101', 'sunday-saturday'),
    ('D20160101', 'tuesday-wednesday,friday'),
])
def test_day_filter(rungroup_str, value):
    filters.DayFilter(value).assert_matches(RunGroup.from_string(rungroup_str))


@pytest.mark.parametrize("rungroup_str, value", [
    ('H20160101:04', '01'),
    ('H20160101:00', '01-23'),
    ('H20160101:02', '23-01'),
    ('H20160101:15', '01-12,14,16,23'),
])
def test_not_in_hour_filter(rungroup_str, value):
    with pytest.raises(filters.NotInSchedule):
        filters.HourFilter(value).assert_matches(RunGroup.from_string(rungroup_str))


@pytest.mark.parametrize("rungroup_str, value", [
    ('H20160101:04', None),
    ('H20160101:04', '04'),
    ('H20160101:00', '22-01'),
    ('H20160101:15', '01-12,15,16,23'),
])
def test_hour_filter(rungroup_str, value):
    filters.HourFilter(value).assert_matches(RunGroup.from_string(rungroup_str))
