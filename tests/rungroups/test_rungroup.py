from datetime import datetime, timedelta

import pytest

from scheduler.rungroups.models import END_OF_TIME, START_OF_TIME, RunGroup


@pytest.mark.parametrize("rungroup_str, starts_at, period", [
    ("adhoc", START_OF_TIME, END_OF_TIME - START_OF_TIME),
    ("service", START_OF_TIME, END_OF_TIME - START_OF_TIME),
    ("D20160101", datetime.strptime("20160101", "%Y%m%d"), timedelta(days=1)),
    ("H20160101:00", datetime.strptime("20160101", "%Y%m%d"), timedelta(hours=1)),
])
def test_from_string(rungroup_str, starts_at, period):
    r = RunGroup.from_string(rungroup_str)
    assert r.name == rungroup_str
    assert r.starts_at == starts_at
    assert r.period.duration == period
