try:
    from scheduler.settings import *
except ImportError:
    pass

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': ':memory:'
    }
}

# Makes sure all tasks are executed synchronously
CELERY_TASK_ALWAYS_EAGER = True
CELERY_BROKER_BACKEND = 'memory'

CONFIG['routing'] = {
    'default-router': 'tests.mock.router.MockRouter',
    'callback-url': 'http://localhost:7777/api/',
    'routers': {
        'tests.mock.router.MockRouter': {
            'version': 'local',
            'url': None
        },
        'tests.mock.router.MissingRouter': {
            'version': 'local',
            'url': None
        },
        'tests.mock.router.SilentRouter': {
            'version': 'local',
            'url': None
        },
        'tests.mock.router.DiscoveryRouter': {
            'version': 'local',
            'url': None
        }
    }
}

CONFIG['timings']['local'] = True

TEST_PROCS_REPO = 'file:///Users/nickgates/kaizen/example-procs'
TEST_PROCS_ROOT = 'tests/'
