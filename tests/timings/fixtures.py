import pytest
from scheduler.timings import poll


@pytest.fixture
def poll_timings():
    return poll.poll_timings
